import os
import pdb
import pandas as pd

import igraph

### This piece of code contains methods necessary to group MHC-II ligands
### into nested sets and to partition those nested sets into train, tune,
### and test holdout sets and save that to disk. These files can then
### be used directly in the model building process.


def partition_peptide_list(peptides=None, allele=None, save_dir=None):
    """
    Given a list of peptides and an indicated allele, group the peptides
    into nested sets, partition the nested sets into train, tune, test
    partitions and then save files in format that model building can use.
    """

    required_phrase = 'I have read and agree to the license contained in this repository'
    print('\nTo affirm that you have read the license associated with this code')
    print('please enter the following phrase (followed by return):\n')
    user_input = input(required_phrase + '\n\n')
    
    
    assert user_input == required_phrase, 'user must enter phrase in prompt to run code...'
        
    df_in = pd.DataFrame(data={'sequence': peptides})

    if allele is None:
        print('allele not provided...')
        allele = 'not_provided'

    # group peptides into nested sets
    df_nest = get_nest_from_cleaned_data(df_in=df_in, allele=allele)
    df_nest['nest_peps'] = df_nest['nest_peps'].apply(lambda x: x.split(';'))
    df_nest['longest_pep'] = df_nest['nest_peps'].apply(lambda x: max(x, key=len))
    # identify genes that peptides could be derived from
    proteome = get_tx_to_seq_dict()
    df_nest['txs'] = \
        df_nest['longest_pep'].apply(lambda x: do_tx_lookup(input_pep=x,
                                                            tx_dict=proteome))

    # cluster peptides into train/tune/test based on gene clustering
    train_txs, tune_txs, test_txs = get_gene_clusters()
    df_nest['is_train'] = \
        df_nest['txs'].apply(lambda x: tx_overlap(list1=x, list2=train_txs))
    df_nest['is_tune'] = \
        df_nest['txs'].apply(lambda x: tx_overlap(list1=x, list2=tune_txs))
    df_nest['is_test'] = \
        df_nest['txs'].apply(lambda x: tx_overlap(list1=x, list2=test_txs))
    
    # save resulting files for model training
    if save_dir is not None:
        if save_dir[-1] != '/':
            save_dir += '/'
    else:
        save_dir = os.getcwd() + '/'

    df_nest['short_peptide'] = df_nest['nest_peps'].apply(lambda x: min(x, key=len))
    df_nest['nest_peps'] = df_nest['nest_peps'].apply(lambda x: ';'.join(x))
    df_nest.rename(columns={'nest_peps': 'peptides'}, inplace=True)
    df_nest['allele'] = allele
    save_cols = ['peptides', 'short_peptide', 'allele']
    train_file = save_dir + allele + '_train.tab'
    df_nest.loc[df_nest['is_train'] == 1][save_cols].to_csv(train_file, sep='\t', index=False)
    tune_file = save_dir + allele + '_tune.tab'
    df_nest.loc[df_nest['is_tune'] == 1][save_cols].to_csv(tune_file, sep='\t', index=False)
    test_file = save_dir + allele + '_test.tab'
    df_nest.loc[df_nest['is_test'] == 1][save_cols].to_csv(test_file, sep='\t', index=False)

    
    return



def get_nest_from_cleaned_data(df_in=None, pep_col='sequence', allele='testing'):
    """
    Given a dataframe with cleaned data it in, generate a dataframe with the
    nested sets... 
    """

    # remove duplicates, just in case
    df_in = df_in.drop_duplicates(pep_col)
    pep_list = list(df_in[pep_col].unique())
    edges = get_edge_info(peps1=pep_list, allele=allele)
    df_nest = do_clustering(edge_list=edges)
    
    return df_nest


def do_clustering(edge_list=None, pep1_col='peptide1', pep2_col='peptide2'):
    """
    The goal of this method is to take in a list of edges
    and return a dataframe that has one row per cluster and
    in each row lists all the peptides in that cluster.
    """

    if type(edge_list) == list:
        peptide1 = [x[1] for x in edge_list]
        peptide2 = [x[2] for x in edge_list]
        edge_list = pd.DataFrame({pep1_col:peptide1, pep2_col:peptide2})
    
    # create a new graph
    our_graph = igraph.Graph()
    # each peptide is a vertex in our graph. add these vertices
    vertices = edge_list[pep1_col].unique()
    our_graph.add_vertices(vertices)
    # each row in edge_list describes the pair of peptides that share an edge.
    # add these edges to our graph
    our_graph.add_edges(es=edge_list[[pep1_col, pep2_col]].as_matrix())
    # perform clustering on the graph
    clustered_graph = our_graph.clusters()
    # this will tell us, for each vertex, what cluster it is in
    members = clustered_graph.membership

    # create dataframe where one column is the cluster_id and the other is
    # the peptide. one row per peptide, indicates what cluster its in.
    df = pd.DataFrame({'cluster_id': members, 'nest_peps': vertices})

    # we want one row per cluster and then a list of all the peptides
    # in that cluster. group by cluster id, join peptides.
    df_gb = df.groupby('cluster_id')['nest_peps'].apply(lambda x: ';'.join(x)).reset_index()
    
    return df_gb


def get_edge_info(peps1=None, peps2=None, K=9, allele='DRB1_0101'):
    """
    Given two lists of peptides, define all the edges
    that exist between them where an edge is said to
    exist between two peptides if they share Kmer (K=9 here by defult).
    """

    if peps2 is not None:
        all_hits = list(set(peps1 + peps2))
    else:
        all_hits = list(set(peps1))
    # dictionary that, for each peptide, has all the kmers within it
    pep2shortmers = {}
    for pep in all_hits:
        L = len(pep)
        pep2shortmers[pep] = []
        # we get 1 Kmer if the peptide has length = K
        for ii in range(L-K+1):
            pep2shortmers[pep].append(pep[ii:(ii+K)])

    for pep in all_hits:
        pep2shortmers[pep] = set(pep2shortmers[pep])
            
    # identify edges
    # loop through all pairs of peptides.
    edges = []
    for pep1 in all_hits:
        for pep2 in all_hits:
            # only check each pair once, also compare each
            # peptide against itself
            if pep1 <= pep2:
                # get all possible shortmers for both peptides
                shortmers1 = pep2shortmers[pep1]
                shortmers2 = pep2shortmers[pep2]
                # check for overlap
                overlap = shortmers1.intersection(shortmers2)
                if len(overlap) > 0:
                    edges.append((allele, pep1, pep2))
    
    return edges


def get_gene_clusters():
    """
    Return list of genes associated with the train, tune, and test
    partitions.
    """

    cluster_file_train = 'gene_clusters_train.txt'
    cluster_file_tune = 'gene_clusters_tune.txt'
    cluster_file_test = 'gene_clusters_test.txt'

    df_train = pd.read_csv(cluster_file_train, sep='\t', names=['txs', 'genes'])
    df_train['txs'] = df_train['txs'].apply(lambda x: x.split(';'))
    df_train['genes'] = df_train['genes'].apply(lambda x: x.split(';'))
    df_tune = pd.read_csv(cluster_file_tune, sep='\t', names=['txs', 'genes'])
    df_tune['txs'] = df_tune['txs'].apply(lambda x: x.split(';'))
    df_tune['genes'] = df_tune['genes'].apply(lambda x: x.split(';'))
    df_test = pd.read_csv(cluster_file_test, sep='\t', names=['txs', 'genes'])
    df_test['txs'] = df_test['txs'].apply(lambda x: x.split(';'))
    df_test['genes'] = df_test['genes'].apply(lambda x: x.split(';'))

    train_list_of_lists = list(df_train['txs'].values)
    train_tx_list = [item for sublist in train_list_of_lists for item in sublist]
    train_tx_list = list(set(train_tx_list))
    tune_list_of_lists = list(df_tune['txs'].values)
    tune_tx_list = [item for sublist in tune_list_of_lists for item in sublist]
    tune_tx_list = list(set(tune_tx_list))
    test_list_of_lists = list(df_test['txs'].values)
    test_tx_list = [item for sublist in test_list_of_lists for item in sublist]
    test_tx_list = list(set(test_tx_list))

    
    return train_tx_list, tune_tx_list, test_tx_list


def tx_overlap(list1=None, list2=None):
    """
    Given two lists, determine if there are overlapping elements
    """

    overlap = set(list1).intersection(set(list2))
    if len(overlap) > 0:
        answer = 1
    else:
        answer = 0
        
    return answer
    

def get_tx_to_seq_dict(fasta_file=None):
    """
    Return dictionary where the key is a transcript isoform id and the
    value is the amino acid sequence of the corresponding protein.
    """
    
    if fasta_file is None:
        fasta_file = 'ucsc_proteome.fasta'
        
    proteome = {}
    with open(fasta_file, 'r') as f:
        for line in f:
            if line[0] == '>':
                newkey = line[1:-1]
                proteome[newkey] = ''
            else:
                proteome[newkey] += line.strip()
                
            
    return proteome
                                                                                

def do_tx_lookup(input_pep=None, tx_dict=None, keep_one=False):
    """
    This is meant to be used within a lambda function. Given an input
    peptide sequence, return all transcript ids that it could be
    derived from
    """

    
    if tx_dict is None:
        # we need a dictionary that will take us from transcript id to sequence
        tx_dict = get_tx_to_seq_dict()
        
    txs = []
    for tx in tx_dict.keys():
        if input_pep in tx_dict[tx]:
            txs.append(tx)
            
    if keep_one:
        if len(txs) > 1:
            txs = txs[0]
            
    return txs
                                                                                            
