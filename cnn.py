import os
import numpy as np
import itertools
import click
import glob
import pdb
import pandas as pd
import sys
from datetime import date
from subprocess import Popen, PIPE

from neonmhc2 import generic_scoring_prank

import setup_code

### This piece of code contains methods relevant to training and calibrating
### neonmhc2-style CNNs for predicting HLA-II binding.


def build_cnns(do_submit=False, audit_mode=False, alleles=None,
               l2s=None, filter_strings=None, model_tags=None,
               jobs_to_submit=None, model_string=None,
               dir_name=None, data_dir=None, kernel_strings=None):
    """
    This method should submit jobs to the grid in order to build CNN model
    ensembles for each class II DR allele. 

    OUTPUTS - A list of "jobs" that could be sent to the grid. The list actually
              just contains information that would be used in constructing a
              job string to feed to our .job file that actually is submitted to
              the grid. We submit it once for each model configuration. Right now,
              There are 35 alleles, 10 model iterations, 4 regularization settings
              resulting in 1400 job submissions and trained models. 

    INPUTS:
    do_submit - This is a boolean that indicates if you want to actually try
                to submit jobs to the grid. If this is false, the "job string"
                that would have been used to submit the job will be printed to the
                screen. This will let you check by eye that the to-be submitted jobs
                are indeed what you wanted to submit.

    audit_mode - In audit mode, the code will verify that models were indeed trained
                 for each of the jobs that you submitted. In audit mode, a list of
                 skipped jobs are returned to the user. This list can then be used
                 as an input to this method (with audit_mode = False, do_submit = True)
                 to attempt retraining the skipped models. Alternatively, if audit_mode
                 is true and do_submit is true then skipped models will be resubmitted
                 they are identified, no need to resubmit. 

    alleles - This is a list of alleles that you want to train models for. By default,
              the code will check for what alleles have data in data_dir.

    l2s - This is a list of regularization strengths that we want to train models with. By
          default, this will use l2s = ['10', '3', '1', '0.5', '0.1']

    filter_strings - This is a list of "filter strings" to train models for. They should be
                     strings of the following format "<N_C1>-<N-C2>", where N_C1 is the number
                     of channels in the first convolutional layer. The default value is "50-50",
                     meaning that 50 channels are used in each of two convolutional layers.

    kernel_strings - This is similar to "filter_strings" but, instead of indicating the number
                     of channels in each convolutional layer, it indicates the kernel size in
                     each convolutional layer. The default value is ['6-6'], indicating
                     two convolutional layers, each with a kernel of 6 amino acids.

    model_tags - This is a list of "model_tags" that distinguish repeated iterations from each
                 other. By default, we train 10 models, each with a tag of "iteration_<n>_of_10_",
                 where n is the iteration number. Each model iteration is saved in a separate
                 directory and the model tag is part of the directory name.

    jobs_to_submit - By default, when you run the code, it will create a list for each
                     job that is to be submitted. The items in the list for a particular
                     job will be: 
                     job_n = [<allele>, <l2 strength>, <filter_string>, <model_tag>]
                     The "jobs_to_submit" will just be a list of these jobs, like
                     [job_1, job_2, ..., job_N]. If you provide a list of jobs, then those
                     are the ones that are submitted and all other input lists are
                     ignored. If you run the code in audit mode, it will output a list of jobs
                     that are skipped. This list can be directly input here in order to just
                     try to retrain those jobs.

    dir_name - This the directory where the models will be saved. By default, we will generate this
               directory name and have it include the date so that the latest models can
               be identified. You should be careful, though, if you incompletely train a set of
               models. In that case, you either want to 
               a) complete the training, fill in missing models,
               b) specify the most up-to-date *complete* set of models to the scoring code or
               c) delete the directory containing the incompletely-trained models.

    data_dir - Specifies where the partitioned data lives. This is the data that you
               generated when you used nested_sets.partition_peptide_list()


    DEFAULT BEHAVIOR
    If you run the code with all defaults, it will NOT submit jobs to the grid, but will give you
    a list of the jobs it could submit in order to build the standard ensemble. This will build
    models for all DR/DP/DQ alleles that are in the most recent data freeze with 10 iterations
    each, at 4 different regularization strengths. To actually build the models,
    run this method with just the "do_submit" setting set to True:

    > build_cnns(do_submit=True, dir_name=<output model dir.>)

    BUILDING SKIPPED JOBS:

    > skipped_jobs = build_cnns(audit_mode=True, do_submit=True, dir_name=<output modle dir.>)

    """

    required_phrase = 'I have read and agree to the license contained in this repository'
    print('\nTo affirm that you have read the license associated with this code')
    print('please enter the following phrase (followed by return):\n')
    user_input = input(required_phrase + '\n\n')

    
    assert user_input == required_phrase, 'user must enter phrase in prompt to run code...'
    
    if alleles is None:
        # if a list of alleles is not provided, we look for what alleles have
        # data in your data directory
        if data_dir is None:
            # if data directory is not provided, we assume current working directory
            data_dir = os.getcwd() + '/'
        alleles = setup_code.get_available_alleles(data_dir=data_dir)

    # this is a list of regularization strengths. They are strings because everything
    # gets interpreted as a string when you submit jobs this way.
    if l2s is None:
        l2s = ['10', '3', '1', '0.5', '0.1']
        
    # this is a list of all the filter strings that we want to try. The default model
    # has two conv layers, each with 50 filters. String format for this is '50-50'.
    if filter_strings is None:
        filter_strings = ['50-50']

    # default model has a kernel of 6 in each of the two conv layers.
    if kernel_strings is None:
        kernel_strings = ['6-6']
        
    # used as a directory identifier for each iteration of the model.
    if model_tags is None:
        n_models = 10
        model_tags = []
        for i in range(n_models):
            model_tags += ['iteration_%d_of_%d_' % (i+1, n_models)]

    if dir_name is None:
        base_dir = os.getcwd() + '/'
        today_string = date.today().strftime('%Y_%m_%d')
        dir_name = base_dir + today_string + '/'
    elif dir_name[-1] != '/':
        dir_name += '/'
        
    # this generates all possible lists that are a combination of one element from each
    # of the lists above. 
    list_of_lists = [alleles, l2s, filter_strings, model_tags, kernel_strings]
    # if you provide a list of jobs, the code will default to that list.
    if jobs_to_submit is None:
        jobs_to_submit = list(itertools.product(*list_of_lists))
    skipped_jobs = []
    for job_to_submit in jobs_to_submit:
        allele = job_to_submit[0]
        l2_factor = job_to_submit[1]
        filter_string = job_to_submit[2]
        model_tag = job_to_submit[3]
        kernel_string = job_to_submit[4]
        
        # h5 tags contain information (in the filename) for models about
        # the filter string and l2 strength. allows us to easily select
        # the file for the best model across these different variables.
        h5_tag = str(l2_factor) + '_' + filter_string
        
        # this is the "job string" described above.
        job_string = \
            'python3 modeling.py --input_alleles %s --l2_factor %s ' % (allele, l2_factor) +\
            '--filter_string %s --model_tag %s --h5_tag %s ' % (filter_string, model_tag, h5_tag) +\
            '--dir_name %s --data_dir %s --kernel_string %s' % (dir_name, data_dir, kernel_string)
        
        if audit_mode:
            model_string = ('%s6-6_50-50_valid_randomseed_3_' % (model_tag) +\
                            'patience_decoy_factor_40_gpool_PDB_singlet_' +\
                            'amino_groups_missing_char_pep_length/')


            # this simply looks at what .h5 files are saved in the expected directory
            # with for the appropriate allele with the appropriate h5 tag.
            built_models = glob.glob(dir_name + model_string + '%s*%s.h5' % (allele, h5_tag))
            if len(built_models) == 0:
                # if we dont find anything, it means no models for this parameter setting
                # have been trained yet. Add this parameter setting to the list of skipped models.
                skipped_jobs += [job_to_submit]

                # if we have do_submit = True, we will submit this skipped job to the grid
                if do_submit:
                    Popen(job_string, shell=True)
        else:
            if do_submit:
                Popen(job_string, shell=True)
            else:
                # print out the job string that would have been used.
                print(job_string)


    # provide basic information on how many jobs would have been/were submitted
    # and how many were skipped if you are running in audit mode.
    n_jobs = len(jobs_to_submit)
    n_skipped_jobs = len(skipped_jobs)
    if audit_mode:
        return_jobs = skipped_jobs
        print('out of %d jobs, you skipped %d...' % (n_jobs, n_skipped_jobs))
    else:
        return_jobs = jobs_to_submit
        print('you are thinking of/actually submitting %d jobs...' % n_jobs)
        
    
    return return_jobs


def score_ref_peptides(model_base_dir=None, model_string=None, do_submit=False,
                       audit_mode=False, model_alleles=None, data_dir=None,
                       alleles_to_score=None):
    """
    The purpose of this method is to perform "calibration" on the models that
    were just built. The process of calibration involves taking each model iteration
    that was built and scoring a set of reference peptides and saving a mapping
    from model score <--> percentile rank within the reference peptides. This way,
    when we score peptides at apply time, we can provide a percent rank instead of
    a raw model score.
    
    INPUTS:
    model_base_dir - This is the physical directory of the model ensemble that the
                     reference peptides will be scored with. This should be the directory
                     that contains all of the iterations in the ensemble. This should
                     literally be the "dir_name" value used when building the models.
                     This should be provided by you when performing calibration.

    model_string - (Optional) This is a string describing the settings used when building
                   the particular model. Models are saved in a directory that contains
                   this string and also a string indicating the model iteration number.
                   This is optional and is only required if you trained models with
                   non-default parameters. 
                   
    do_submit - Boolean indicating if you truly want to submit the scoring.

    audit_mode - Boolean indicating if you are checking to see what alleles were skipped.
    """

    assert model_base_dir is not None, 'please provide model directory...'
    if model_base_dir[-1] != '/':
            model_base_dir += '/'
    if model_string is None:
        # this describes the type of model that was built. If you manually edit the
        # model building code, then you will need to pass a different model string
        # to this function. This is the one that will be valid by default
        model_string = '6-6_50-50_valid_randomseed_3_patience_decoy_factor_' +\
                       '40_gpool_PDB_singlet_amino_groups_missing_char_pep_length'

    assert data_dir is not None, 'please provide path to training data...'
    if data_dir[-1] != '/':
        data_dir += '/'        
        
    # this gives us the alleles that we have models for, takes into account
    # model path provided.
    if model_alleles is None:
        model_alleles = \
            setup_code.get_latest_allele_list(model_base_dir=model_base_dir,
                                              model_string=model_string)

    if alleles_to_score is None:
        alleles_to_score = model_alleles

    skipped_alleles = []
    for allele in alleles_to_score:
        job_string = \
            'python3 cnn.py --allele %s --model_base_dir %s ' % (allele, model_base_dir) +\
            '--model_string %s --data_dir %s' % (model_string, data_dir)
        
        if audit_mode:
            # this goes into the directory for each of the model iterations and
            # checks for the saved score <--> percentile mapping.
            allele_check = check_for_scored_peptides(allele=allele,
                                                     model_base_dir=model_base_dir,
                                                     model_string=model_string)
            if allele_check == False:
                skipped_alleles += [allele]
                if do_submit:
                    Popen(job_string, shell=True)
        else:

            if do_submit:
                Popen(job_string, shell=True)
            else:
                print(job_string)

    if audit_mode:
        return_alleles = skipped_alleles
        print('skipped %d (of %d) alleles...' % (len(skipped_alleles),
                                                 len(alleles_to_score)))
    else:
        return_alleles = alleles_to_score

                
    return return_alleles


@click.command()
@click.option('--allele')
@click.option('--model_base_dir')
@click.option('--model_string')
@click.option('--data_dir')
def score_wrapper(allele=None, model_base_dir=None, model_string=None, data_dir=None):
    """
    This is just a wrapper for the score_reference_peptides() method. We will call this
    when submitting jobs, and use score_reference_peptides() for other use cases.
    """

    assert model_base_dir is not None, 'please provide model directory...'
    if model_base_dir[-1] != '/':
        model_base_dir += '/'
    if model_string is None:
        model_string = '6-6_50-50_valid_randomseed_3_patience_decoy_factor_' +\
                       '40_gpool_PDB_singlet_amino_groups_missing_char_pep_length'
                
    score_reference_peptides(allele=allele, model_base_dir=model_base_dir,
                             model_string=model_string, data_dir=data_dir)
    

    return


def score_reference_peptides(allele=None, model_base_dir=None, model_string=None,
                             data_dir=None):
    """
    Given a specified model, score the set of reference peptides with that model...
    """

    
    pep_dir = os.getcwd() + '/'
    pep_file = 'reference_peptides.tab'
    df = pd.read_csv(pep_dir + pep_file, sep='\t', names=['peptide'])
    df['allele'] = allele
            
    # here, we score the file with the appropriate model. The "save_apply"
    # setting will save the scored peptides in the individual model directories
    # so that they can be loaded for future scoring.
    df_score = generic_scoring_prank(df_pep=df, allele=allele,
                                     model_base_dir=model_base_dir,
                                     model_string=model_string,
                                     save_apply=True,
                                     ptile_lookup=False,
                                     data_dir=data_dir)

    
    return


def check_for_scored_peptides(allele=None, model_base_dir=None, model_string=None,
                              n_models=10):
    """
    Check if the reference peptides have been scored for all iterations of a model
    for a given allele.
    """

    scores_found = True
    for i in range(1,(n_models+1)):
        model_dir = model_base_dir + 'iteration_%d_of_10_' % i
        model_dir = model_dir + model_string + '/'

        scored_ref_files = glob.glob(model_dir + '*reference_percentiles*%s.csv' % allele)

        if len(scored_ref_files) == 0:
            scores_found = False

    
    return scores_found


if __name__ == "__main__":
    score_wrapper()
