from collections import Counter
import itertools
import numpy as np
import pandas as pd
import random
import pdb
import sys
import os

### These methods are relevant for featurizing peptides. Please note,
### default values given in the function definitions do not generally
### reflect the values that are used when building the default model.
### To see the values that are used, refer to where the methods
### are called in the model building process.



def get_default_feature_selection_dict():
    """
    This is a vestige of a larger code base incorporating
    functionality beyond what was published in the paper.
    """

    feature_dict = {}
    feature_dict['PDB_singlet'] = True
    feature_dict['amino_groups'] = True
    feature_dict['missing_char'] = True
    feature_dict['pep_length'] = True

    
    return feature_dict


def shorten_peptide(peptide=None, max_length=30,
                    apply_centering=False):
    """
    Method to shorten peptides consistently in all places. This just shortens
    the peptide strings. The model, by default, scores the central
    20mer of peptides that are longer than 20AA.
    """

    if len(peptide) > max_length:
        if apply_centering:
            i_center = np.floor(len(peptide)/2).astype(int)
            i_left = np.floor(i_center - max_length/2).astype(int)
            i_right = np.floor(i_center + max_length/2).astype(int)
            peptide = peptide[i_left:i_right]
        else:
            peptide = peptide[0:max_length]
        
    return peptide
        

def featurize_dataframe(df_pep=None, peptide_col='peptide', label_col='label',
                        feature_change_dict=None, max_length=30, center_pad=True,
                        include_weights=True, weight_col='weights',
                        sym_short=False, include_means=True):
    """
    Provided a dataframe that has peptides in one column (peptide_col)
    and label values in another column (label_col), return the original
    dataframe along with numpy array of featurized peptides and a numpy
    array of the labels. 

    The chosen features are provided via a dictionary (feature_change_dict)
    where the key is the feature "name" and the value is a Boolean indicating
    if that feature set is included in the featurization.
    """

    include_means = True
    
    if include_means:
        seq_dict, pep_dict = get_aa_freq_dict()
    else:
        seq_dict = None
        pep_dict = None
        
    if 'weights' not in list(df_pep.columns.values):
        print('setting weights to 1... if you are relying on weights')
        print('you may want to look into this...')
        df_pep['weights'] = 1

    print('featurization...')
    
    feature_dict = get_default_feature_selection_dict()
    if feature_change_dict is not None:
        for key in list(feature_change_dict.keys()):
            feature_dict[key] = feature_change_dict[key]

    # here we start getting features and aggregating them
    have_any_feats = 0
    have_aa_data = 0
    if feature_dict['PDB_singlet'] == True:
        print('\tincorporating PDB singlet features...')
        aa_temp_data, aa_label = add_pdb_properties(df_mr=df_pep, max_length=max_length,
                                                        center_pad=center_pad,
                                                        peptide_col=peptide_col,
                                                        label_col=label_col,
                                                        include_means=include_means,
                                                        seq_dict=seq_dict, pep_dict=pep_dict)
        if have_any_feats:
            aa_data = combine_feature_arrays(array1=aa_data, array2=aa_temp_data)
        else:
            have_any_feats = 1
            aa_data = np.copy(aa_temp_data)
    if feature_dict['amino_groups'] == True:
        print('\tincorporating amino acid property features...')
        aa_df_file = 'amino_acid_venn_diagram.tsv'
        aa_temp_data, aa_label = add_generic_properties(df_gen=df_pep, max_length=max_length,
                                                        center_pad=center_pad,
                                                        df_file=aa_df_file,
                                                        df_sep='\t',
                                                        aa_col='amino_acid',
                                                        peptide_col=peptide_col,
                                                        label_col=label_col,
                                                        include_means=include_means,
                                                        seq_dict=seq_dict, pep_dict=pep_dict)
        if have_any_feats:
            aa_data = combine_feature_arrays(array1=aa_data, array2=aa_temp_data)
        else:
            have_any_feats = 1
            aa_data = np.copy(aa_temp_data)
    if feature_dict['pep_length'] == True:
        print('\tincorporating amino acid position features...')
        aa_temp_data, aa_label = add_distance_properties(df_dist=df_pep, max_length=max_length,
                                                         center_pad=center_pad,
                                                         peptide_col=peptide_col,
                                                         label_col=label_col)
        if have_any_feats:
            aa_data = combine_feature_arrays(array1=aa_data, array2=aa_temp_data)
        else:
            have_any_feats = 1
            aa_data = np.copy(aa_temp_data)
    if feature_dict['missing_char'] == True:
        print('\tproviding special characters for "missing" amino acids...')
        missing_data = gen_missing_aa_indicator(X_miss=aa_data)
        aa_data = combine_feature_arrays(array1=aa_data, array2=missing_data)

    if include_weights:
        pep_weights = df_pep[weight_col].values
    else:
        pep_weights = np.ones(np.shape(aa_label))
        
    return aa_data, aa_label, pep_weights, df_pep


def get_amino_acids(include_means=False, only_means=False):
    """
    Get a list of the the acceptable amino acids.
    If we are including missing, then we add X for missing
    amino acids and B for binder amino acids.
    """

    aas = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
           'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

    if include_means:
        aas += ['B', 'X']

    if only_means:
        aas = ['B', 'X']

    return aas


def add_distance_properties(df_dist=None, max_length=30, center_pad=True,
                            label_col='is_decoy', peptide_col='input_peptide',
                            sym_short=False):
    """
    Create a numpy array that we can append to ther data
    which contains the information about the positions of
    the amino acids within the peptide.
    """

    n_feats = 2
    dist_data = np.zeros((df_dist.shape[0], max_length, n_feats))
    for i in range(df_dist.shape[0]):
        the_peptide = df_dist.iloc[i][peptide_col]
        the_peptide = shorten_peptide(peptide=the_peptide,
                                      max_length=max_length,
                                      apply_centering=sym_short)
        pep_len = len(the_peptide)
        if center_pad:
            pad = np.floor((max_length - pep_len) / 2).astype(int)
        else:
            pad = 0
        # need to calculate this separately if we want it the method
        # to generate a useful feature when center_pad = False
        dpad = np.floor((pep_len) / 2).astype(int)
        for j in range(pep_len):
            dist_data[i, (j+pad), 0] = np.abs(j - dpad)
            dist_data[i, (j+pad), 1] = (j - dpad)

    dist_label = df_dist[label_col].values
    dist_label = np.ravel(dist_label)
            
    return dist_data, dist_label


def add_pdb_properties(df_mr=None, max_length=30, center_pad=True,
                       label_col='is_decoy', peptide_col='input_peptide',
                       sym_short=False, include_missing=True,
                       include_means=False, seq_dict=None, pep_dict=None):
    """
    Add the 'singleton' features that mike made from PDB
    """
    mr_dict = get_pdb_dict()
    if include_means:
        mr_dict = add_missing_aa_feats(feat_dict=mr_dict, seq_dict=seq_dict,
                                       pep_dict=pep_dict)
    n_feats = np.shape(mr_dict['A'])[-1]

    if include_missing:
        missing_val = (mr_dict[list(mr_dict.keys())[0]] + 100) * 0
        mr_dict['-'] = missing_val
    
    mr_data = np.zeros((df_mr.shape[0], max_length, n_feats))
    for i in range(df_mr.shape[0]):
        the_peptide = df_mr.iloc[i][peptide_col]
        the_peptide = shorten_peptide(peptide=the_peptide,
                                      max_length=max_length,
                                      apply_centering=sym_short)
        pep_len = len(the_peptide)
        if center_pad:
            pad = np.floor((max_length - pep_len) / 2).astype(int)
        else:
            pad = 0
        for j in range(pep_len):
            aa = the_peptide[j]
            the_feats = mr_dict[aa]
            mr_data[i, (j+pad), :] = the_feats

    mr_label = df_mr[label_col].values
    mr_label = np.ravel(mr_label)
            
    return mr_data, mr_label
    

def add_generic_properties(df_gen=None, max_length=30, center_pad=True,
                           df_file=None, df_sep='|', aa_col='amino_acid',
                           label_col='is_decoy', peptide_col='input_peptide',
                           sym_short=False, include_missing=True,
                           include_means=False, seq_dict=None, pep_dict=None,
                           do_transpose=False):
    """
    Take features from a generic dictionary and apply
    this featurization to the peptides in df_gen.
    """

    gen_dict = get_generic_dict(df_filename=df_file, df_sep=df_sep, aa_col=aa_col,
                                do_transpose=do_transpose)
    if include_means:
        gen_dict = add_missing_aa_feats(feat_dict=gen_dict, seq_dict=seq_dict,
                                        pep_dict=pep_dict)
    n_feats = np.shape(gen_dict['A'])[1]

    if include_missing:
        missing_val = (gen_dict[list(gen_dict.keys())[0]] + 100) * 0
        gen_dict['-'] = missing_val
    
    gen_data = np.zeros((df_gen.shape[0], max_length, n_feats))
    for i in range(df_gen.shape[0]):
        the_peptide = df_gen.iloc[i][peptide_col]
        the_peptide = shorten_peptide(peptide=the_peptide,
                                      max_length=max_length,
                                      apply_centering=sym_short)
        pep_len = len(the_peptide)
        if center_pad:
            pad = np.floor((max_length - pep_len) / 2).astype(int)
        else:
            pad = 0
        for j in range(pep_len):
            aa = the_peptide[j]
            the_feats = gen_dict[aa]
            gen_data[i, (j+pad), :] = the_feats

    gen_labels = df_gen[label_col].values
    gen_labels = np.ravel(gen_labels)
            
    return gen_data, gen_labels
    

def combine_feature_arrays(array1=None, array2=None):
    n_examples = np.shape(array1)[0]
    n_aas = np.shape(array1)[1]
    n_feats_1 = np.shape(array1)[2]
    n_feats_2 = np.shape(array2)[2]
    combined_data = np.zeros((n_examples, n_aas, n_feats_1+n_feats_2))

    for i in range(n_examples):
        combined_data[i, :, :] = np.hstack((array1[i, :, :], array2[i, :, :]))

    return combined_data


def get_generic_dict(df_filename=None, df_sep=None, aa_col=None, do_transpose=False):
    """
    Take a dataframe with amino acid column and then feature
    columns and turn it into a dictionary.
    """

    df = pd.read_csv(df_filename, sep=df_sep)
    if do_transpose:
        df = df.set_index(aa_col)
        df = df.T
        df[aa_col] = df.index
    feat_cols = list(df.columns.values)
    not_feats = [aa_col]
    feat_cols = [x for x in feat_cols if x not in not_feats]

    aas = df[aa_col].unique()

    gen_dict = {}
    for aa in aas:
        gen_dict[aa] = {}
        gen_dict[aa] = df.loc[df[aa_col] == aa][feat_cols].values
    
    return gen_dict


def gen_missing_aa_indicator(X_miss=None):
    """
    This method will just generate a feature to tell you whether
    or not a given position in the peptide had an amino acid or not.
    """

    missing_bool = np.zeros(np.shape(X_miss.any(axis=2)))
    check = X_miss.any(axis=2, out=missing_bool)
    missing_bool = np.expand_dims(missing_bool, 2)

    return missing_bool


def add_missing_aa_feats(feat_dict=None, seq_dict=None, pep_dict=None):
    """
    This is a method that will add a feature in your dictionary for
    "missing" amino acids (denoted X) and "binder" amino acids
    (denoted B).

    feat_dict - this is the dictionary you would have used for
                featurization
    seq_dict -  this is the dictionary that has amino acid frequencies
                in general
    pep_dict - this is a dictionary that contains amino acid frequencies
               within mass-spec observed binding peptides.
    """

    is_first = True
    aas = get_amino_acids()
    for aa_key in aas:
        missing_array = feat_dict[aa_key] * seq_dict[aa_key]
        binder_array = feat_dict[aa_key] * pep_dict[aa_key]
        
        if is_first:
            missing_feat = missing_array
            binder_feat = binder_array
            is_first = False
        else:
            missing_feat += missing_array
            binder_feat += binder_array
            
        feat_dict['X'] = missing_feat
        feat_dict['B'] = binder_array
        
        
    return feat_dict


def get_pdb_dict():
    """
    Turn the pdb feature dataframe into a dict, probably
    an easier way to do this, but we know this works...
    """

    df_file = 'nn_matrix_normalized.tab'
    df = pd.read_csv(df_file, sep='\t')
    not_feats = ['AA1']
    feat_names = list(df.columns.values)
    feat_names = [x for x in feat_names if x not in not_feats]
    aas = df['AA1'].unique()
    aa_dict = {}
    for aa in aas:
        aa_dict[aa] = {}
        aa_dict[aa] = df.loc[df['AA1'] == aa][feat_names].values
        
    return aa_dict


def get_aa_freq_dict():
    """
    calculate the amino acid frequencies within binders and, separately,
    within binders and their context... this will be used later to get
    the average value of features by amino acid to use for "missing"
    amino acids and masked amino acids within a peptide.
    """

    # we're going to calcualte these frequencies based on source transcripts
    # from peptides from our training data.
    train_file = '70_train_pseudo_cleaned.tsv'
    df_train = pd.read_csv(train_file, sep='\t')
    
    # capitalize everything out of paranoia
    df_train['seq'] = df_train['seq'].apply(lambda x: x.upper())
    
    df_train['seq'] = df_train['seq'].apply(lambda x: x.replace('-', ''))
    df_train['seq'] = df_train['seq'].apply(lambda x: x.replace('X', ''))
    
    seq_string = ''.join(df_train['seq'].unique())
    
    # get counts
    seq_count = Counter(seq_string)
    
    # totals
    seq_total = len(seq_string)
    
    # iterate through amino acids, get proportion
    missing_dict = {}
    for aa in sorted(list(seq_count.keys())):
        missing_dict[aa] = 1.0*seq_count[aa]/seq_total

    # in the end, we decided to use a pre-calculated dictionary
    # of amino acid frequencies inside of observed binders.
    binder_dict = {'A': 0.08952664751025814,
                   'C': 0.0019747655704944007,
                   'D': 0.037185505772543266,
                   'E': 0.056085707190748166,
                   'F': 0.04117051174413976,
                   'G': 0.05746528393660254,
                   'H': 0.024970339099964132,
                   'I': 0.06118619950256404,
                   'K': 0.060409694877040294,
                   'L': 0.10218722038935597,
                   'M': 0.015873015873015872,
                   'N': 0.03765850351397906,
                   'P': 0.05352757773914963,
                   'Q': 0.04855715980622859,
                   'R': 0.059467641042014026,
                   'S': 0.08016129222982961,
                   'T': 0.049806662173188125,
                   'V': 0.07944785396983063,
                   'W': 0.007867529099215217,
                   'Y': 0.03547088895983855}
    
    
    
    return missing_dict, binder_dict
