import pdb
import yaml
import glob, os, re, yaml
import pandas as pd
import numpy as np
import sys
import random

import keras
from keras import backend as K
from keras import regularizers, optimizers, initializers
from keras.models import Sequential
from keras.layers import Conv1D, Dense, MaxPooling1D, Flatten, GlobalMaxPooling1D
from keras.layers import Dropout, SpatialDropout1D, BatchNormalization
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_recall_curve
from keras.models import load_model
from keras.layers import LeakyReLU
import keras.layers
from keras.engine.topology import Layer

from sklearn.model_selection import train_test_split

import processing

class tMaximum(Layer):
    def __init__(self, **kwargs):
        super(tMaximum, self).__init__(**kwargs)
    def build(self, input_shape):
        super(tMaximum, self).build(input_shape)
    def call(self, inputs):
        return K.max(inputs, axis=1, keepdims=True)
    def compute_output_shape(self, input_shape):
        return (input_shape[0], 1)

### The methods below are used in supporting model training and
### model lookups. Please note that the default values shown in the
### function definitions do not generally reflect the values used
### when training models with default parameters. To see what values
### are used, please refer to when these methods are called in
### the model building/scoring code.


def get_training_data(allele='DRB1_0101', max_length=20, feat_change_dict=None,
                      sym_short=False, addl_cols=None, data_dir=None,
                      partition='train', decoy_factor=20, fix_seed=True,
                      sample_size=None):
    """
    Return featurized training data 
    """
    
    df_train = \
        get_shuffled_training_peptides(allele=allele, data_dir=data_dir,
                                       fix_seed=fix_seed, 
                                       sample_size=sample_size)    
    
    df_train = expand_peptide_lists(df_wide=df_train, addl_cols=addl_cols)
    
    train_data, train_labels, train_weights, df_train = \
        processing.featurize_dataframe(df_pep=df_train, max_length=max_length,
                                       feature_change_dict=feat_change_dict,
                                       sym_short=sym_short)
    
    return train_data, train_labels, train_weights, df_train


def get_shuffled_training_peptides(allele='DRB1_0101', decoy_factor=20, fix_seed=True,
                                   data_dir=None,
                                   sample_size=None, partition='train'):
    """ 
    This method should generate training data assuming that we want
    to use shuffled hit peptides as our decoy peptides.
    """
    downsample = False
    if fix_seed:
        random.seed(666)
    else:
        random.seed()

    df = get_partitioned_data(allele=allele, data_dir=data_dir,
                              partition=partition)
        
    n_nests = df.shape[0]
    print('\tidentified %d nested sets for this allele...' % n_nests)
    
    if 'seq' in list(df):
        df['seq'] = df['seq'].apply(lambda x: x.upper())

    # we're going to start with only hits, generate decoys by shuffling
    df = df.loc[df['label'] == 1]
    # split peptide column into a list
    df['peptides'] = df['peptides'].apply(lambda x: x.split(';'))
    df['original_peptides'] = df['peptides']

    df_true = df.copy()
    df_decoy = df.copy()
    df_decoy['label'] = 0
    for i in range(decoy_factor):
        df_decoy['peptides'] =\
            df_decoy['peptides'].apply(lambda x: [''.join(random.sample(s, len(s))) for s in x])
        df_true = df_true.append(df_decoy)
        
    df_true = df_true.reset_index(drop=True)
    
    return df_true


def get_binders_with_shuffling(allele='DRB1_1101', data_dir=None,
                               decoy_factor=20, max_length=20,
                               feat_change_dict=None, sym_short=True,
                               addl_cols=None, fix_seed=True,
                               partition='tune'):
    """
    Given the provided allele, we want to load the tune partition
    of that allele's data from the data_dir and take the shortest
    peptide per nested set, add decoys, apply featurization, return
    a data frame as well as a featurized numpy array, array of labels
    and array of weights.
    """

    if fix_seed:
        random.seed(666)
    else:
        random.seed()

    df_tune = get_partitioned_data(allele=allele, data_dir=data_dir,
                                   partition=partition)
        
    df_tune = df_tune.loc[(df_tune['allele'] == allele) &
                          (df_tune['label'] == 1)]
    df_tune['peptides'] = df_tune['peptides'].apply(lambda x: x.split(';'))
    df_tune['short_peptide'] = df_tune['peptides'].apply(lambda x: min(x, key=len))

    df_true = df_tune.copy()
    df_true['label'] = 1
    df_true['input_peptide'] = df_true['short_peptide']
    df_decoy = df_tune.copy()
    df_decoy['label'] = 0
    if decoy_factor > 0:
        for i in range(decoy_factor):
            df_decoy['input_peptide'] = \
                df_decoy['short_peptide'].apply(lambda x: ''.join(random.sample(x, len(x))))
            df_true = df_true.append(df_decoy)
            df_true = df_true.reset_index(drop=True)
    df_true['group_id'] = df_true.index

    df_true.rename(columns={'short_peptide': 'original_peptide'}, inplace=True)
    df_true.rename(columns={'input_peptide': 'peptide'}, inplace=True)
    df_true['weights'] = 1
    df_true = df_true.reset_index(drop=True)
    df_true['group_id'] = df_true.index
    keep_col = ['peptide', 'label', 'weights', 'allele']
    if addl_cols is not None:
        keep_col += addl_cols
    df_true = df_true[keep_col]

    
    val_data, val_labels, val_weights, df_val =\
        processing.featurize_dataframe(df_pep=df_true, max_length=max_length,
                                       feature_change_dict=feat_change_dict,
                                       sym_short=sym_short, peptide_col='peptide')

    return val_data, val_labels, val_weights, df_val


def get_ensemble_model_list(allele='DRB1_0101', n_models=10,
                            base_dir=None, model_string=None,
                            return_model=True, verbose=False):
    """
    This method is used to locate the appropriate models during apply time.
    Given an allele and the director of a model, we return the best model
    for that allele for each iteration along with the feature dictionary
    that was used to featurize peptides for that model and the model path.
    """

    model_list = []
    feat_dict_list = []
    model_dirs = []
    for i in range(n_models):
        model_tag = 'iteration_%d_of_%d_' % (i+1, n_models)

        model_dir = base_dir + model_tag + model_string
        model_dirs += [model_dir]
        if model_dir[-1] != '/':
            model_dir += '/'
        model, feat_dict = fixed_get_the_right_model(allele=allele,
                                                     model_dir=model_dir,
                                                     return_model=return_model,
                                                     verbose=verbose)
        model_list += [model]
        feat_dict_list += [feat_dict]
    
    return model_list, feat_dict_list, model_dirs


def fixed_get_the_right_model(allele='DRB1_0101', model_dir=None, file_tag='',
                              return_model=True, verbose=True):
    """
    Given a directory (of models) and an allele,
    return the model for the allele which has the highest
    validation PPV
    """

    ppv_index = 0
    
    if file_tag != '':
        if file_tag[-1] != '_':
            file_tag += '_'

    glob_string = '%s%s%s_*.h5' % (model_dir, file_tag, allele)
    model_names = glob.glob(glob_string)

    # everything that isn't the actual h5 file
    model_names = [x.split('/')[-1] for x in model_names]

    # get the validation ppv values
    ppvs = [re.findall("\d+\.\d+", x)[0] for x in model_names]
    model_index = np.argmax(ppvs)
    model_name = model_names[model_index]

    if verbose:
        print('ppv values for all models:', ppvs)
        print('loading model %s...' % model_name)
    if return_model:
        model = load_model(model_dir + model_name)
    else:
        model = model_name
    with open(model_dir+'feature_selection.yaml', 'r') as stream:
        feat_dict = yaml.load(stream)
            
    return model, feat_dict


def expand_peptide_lists(df_wide=None, list_col='peptides', label_col='label',
                         calculate_weights=True, narrow_cols=True, strip_missing=True,
                         addl_cols=None, deduplicate=False):
    """
    This method takes a dataframe that has a "peptides" column containint
    a list of peptides and returns a dataframe with one row per peptide.
    This is useful, for example, when expanding nested sets.
    """

    peptide = \
        df_wide.apply(lambda x: pd.Series(x[list_col]),axis=1).stack().reset_index(level=1,
                                                                                   drop=True)
    expand_col = 'peptide'
    weight_col = 'weights'
    keep_cols = [expand_col, label_col]    
    peptide.name = expand_col
    if deduplicate:
        peptide = peptide.drop_duplicates()
    df_wide = df_wide.join(peptide)

    if calculate_weights:
        df_wide[weight_col] = df_wide[list_col].apply(lambda x: 1.0/len(x))
        keep_cols = keep_cols + [weight_col]

    if addl_cols != None:
        keep_cols = keep_cols + addl_cols
        
    if narrow_cols:
        # if we select "narrow_cols", then we just return the peptide
        # and the label.
        df_wide = df_wide[keep_cols]

    if strip_missing:
        df_wide[expand_col] = df_wide[expand_col].apply(lambda x: x.lstrip('-').rstrip('-'))
        
    return df_wide


def get_latest_allele_list(model_base_dir=None, model_string=None):
    """
    Given a model directory and a model string, return a list of alleles
    that have trained models.
    """

    excluded_alleles = []

    assert model_base_dir is not None, 'please provide model directory...'
    assert model_string is not None, 'must provide model_string...'
    print('determining covered alleles based on model files...')
    print('\t%s...' % model_base_dir)
    print('\t%s...' % model_string)
    
    # get list of all model dirs, one per iteration, then just select one...
    model_files = glob.glob(model_base_dir + '*%s/*.h5' % model_string)
    model_files = [x.split('/')[-1] for x in model_files]
    alleles = ['_'.join(x.split('_')[0:2]) for x in model_files]
    alleles = sorted(list(set(alleles)))

            
    return alleles


def get_available_alleles(data_dir=None):
    """
    Return a list of alleles that have training data in the provied
    directory
    """

    assert data_dir is not None, 'please provide training data directory...'
    train_files = glob.glob(data_dir + '*_train.tab')

    alleles = ['_'.join(x.split('/')[-1].split('_')[0:2]) for x in train_files]

    alleles = sorted(list(set(alleles)))

    return alleles


def get_partitioned_data(allele=None, data_dir=None, partition='train',
                         sample_size=None):
    """
    Given a user-specified directory where partitioned binding data lives,
    load the data corresponding to the allele and partition we are interested in.
    Additionally, allow for option of subsampling data. This will only load
    hits.
    """

    data_file = '%s%s_%s.tab' % (data_dir, allele, partition)
    check_exists = glob.glob(data_file)
    print(data_file)
    assert len(check_exists) > 0, 'unable to find data in provided directory...'

    df_allele = pd.read_csv(data_file, sep='\t')

    if sample_size is not None:
        print('\nsubsampling data, not indended for building full models...')
        df_allele = df_allele.sample(n=sample_size)

    # these are all hits
    df_allele['label'] = 1
        
    return df_allele


def scan_across_peps(df_in=None, pep_col='peptide', l_min=13, l_max=25, fill_null=False,
                     deduplicate=False):
    """
    Get substrings of each peptide of length l_min <= L <= l_max
    """
    
    if 'label' not in list(df_in.columns.values):
        delete_later = True
        df_in['label'] = 'delete'
    else:
        delete_later = False
        
    df_in['peptides'] = df_in[pep_col].apply(lambda x: get_subLmers(peptide=x,
                                                                    l_min=l_min,
                                                                    l_max=l_max))
    
    if 'original_peptide' not in list(df_in.columns.values):
        df_in.rename(columns={pep_col: 'original_peptide'}, inplace=True)
    else:
        if 'peptide' in list(df_in.columns.values):
            df_in.rename(columns={'peptide': 'pre_expanded_peptide'},
                         inplace=True)
    df_in['group_id'] = df_in.index
    addl_cols = list(df_in.columns.values)
    
    df_in = expand_peptide_lists(df_wide=df_in, addl_cols=addl_cols,
                                 calculate_weights=False, strip_missing=False,
                                 deduplicate=deduplicate)
    
    if delete_later:
        del df_in['label']
        
    if fill_null:
        df_in.loc[df_in['peptide'].isnull(), 'peptide'] = \
            df_in.loc[df_in['peptide'].isnull(), 'original_peptide']
            
    return df_in


def get_subLmers(peptide=None, l_min=9, l_max=20):
    """
    Get all the l_min <= L <= l_max sub-peptides for a given peptide
    """
    
    pep_list = []
    for l_sub in range(l_min, (l_max+1)):
        max_i = len(peptide) - l_sub + 1
        if max_i > 0:
            addl_pep_list = list(set([peptide[i:(i+l_sub)] for i in range(max_i)]))
            pep_list += addl_pep_list
        
    return pep_list
