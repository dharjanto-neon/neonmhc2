import click
import os
import pdb
import sys
import yaml
import pandas as pd
import numpy as np
from tensorflow import set_random_seed

import keras
from keras import backend as K
from keras import regularizers, optimizers, initializers
from keras.models import Sequential
from keras.layers import Conv1D, Dense, MaxPooling1D, Flatten, GlobalMaxPooling1D
from keras.layers import Dropout, SpatialDropout1D, BatchNormalization
from sklearn.metrics import confusion_matrix
from keras.models import load_model
from keras.layers import LeakyReLU
import keras.layers
from keras.engine.topology import Layer

import processing
import setup_code as gc


@click.command()
@click.option('--input_alleles')
@click.option('--model_tag')
@click.option('--l2_factor')
@click.option('--h5_tag')
@click.option('--filter_string')
@click.option('--kernel_string')
@click.option('--dir_name')
@click.option('--data_dir')
def train_wrapper(filter_string=None, kernel_string=None,
                  data_dir=None, input_alleles=None, model_tag='',
                  l2_factor=1, h5_tag=None, dir_name=None, sample_size=None):
    """
    This is just a wrapper for the method that will actually train the models.
    when building models in production, we will be using the code from the
    command line and this method will be called.
    """

    train_iteration(input_alleles=input_alleles, model_tag=model_tag,
                    l2_factor=l2_factor, h5_tag=h5_tag,
                    filter_string=filter_string, dir_name=dir_name,
                    sample_size=sample_size, data_dir=data_dir,
                    kernel_string=kernel_string)
    

    return

    
def train_iteration(filter_string=None, kernel_string=None, data_dir=None,
                    input_alleles=None, model_tag='', max_bad_epochs=3,
                    check_val_every_n_epochs=5, max_ppv_lowering_epochs=3,
                    max_length=20, l2_factor=1, use_dropout=True,
                    tune_decoy_factor=20, dropout_rate=0.2, lr_decay=3,
                    h5_tag=None, save_intermediate_models=False,
                    dir_name=None, sample_size=None, base_lr=None):

    """
    Train a single model iteration for the ensemble. 

    OUTPUTS:
    There are no important outputs that are provided if you are running this
    within ipython3. However, as this runs, it will save important model
    files. This includes feature normalization parameters, feature choices,
    keras-readable model files, and also scores on the tune partition.

    INPUTS:
    filter_string - This is a string which contains hyphen-separated channel
                    counts for the network. For example, the default network
                    that we are building has two convolutional layers with 50
                    channels/filters in each layer. This would be represented
                    as '50-50' (string)

    kernel_string - This is similar to the filter string but, instead of
                    indicating the number of channels in each convolutional
                    layer, it indicates the kernel size in each convolutional
                    layer. The default value (loaded below) is '6-6', indicating
                    two convolutional layers, each with a kernel of 6 amino acids.
                    (string)
    
    data_dir - This is the directory where your training data lives. The data
               must be saved in a file named <allele>_<train/tune/test>.tab.
               This is the format used by the provided code that partitions
               the data (nested_sets.py).

    input_alleles - This is the allele that we are training a model for.
                    For example, 'DRB1_0101', ['DRB1_0101', 'DRB1_0301'].
                    Must take THIS format, no "*" or ":'. 
                    (string or list of strings)                    
    
    model_tag - When building an ensemble of models, this will be a string that
                indicates which iteration of the model we are on. An example
                value would be 'iteration_2_of_10_' (string)

    max_bad_epochs - This describes the maximum number of consecutive training
                     epochs we are willing to tolerate without the training loss
                     dropping sufficiently before we terminate training. (int)

    check_val_every_n_epochs - Indicates how often we want to evaluate our model
                               performance on the tune partition. This is involved
                               in a couple things. First, it is relevant for the
                               early-stopping scheme. If the validation PPV (PPVval)
                               failes to improve after a designated number of checks,
                               we terminate training. Also, we keep a variable
                               filled with the best-performing model during training.
                               Every time we check the validation performance,
                               we also update the variable that holds the best
                               performing model. (int)

    max_ppv_lowering_epochs - This is relevant to the input above. If PPVval has
                              failed to improve after. This number of checks, then
                              we terminate training. (int)

    max_length - This indicates how many amino acids we want to featurize, at most,
                 for each input peptide. The default value is 20 mean that:
                 a) for peptides shorter than N amino acids, we will pad the edges
                    symmetrically with zeros and
                 b) for peptides longer than N amino acids, we will select the central
                    N of them. (int)

    l2_factor - This is the amount of L2 regularization we would like to incorporate
                into the model. The precise amount applied to the weights within
                the network varies by layer, but this controls the overall magnitude.
                (float)

    dropout_rate - This indicates the amount droptout we should use. Reflects the
                   proportion of channels that randomly have their activations set
                   to zero during each application of the model while training.
                   (float)
    
    lr_decay - We decrease the learning rate every time the training error fails to
               decrease by a sufficiently large amount. The updated learning rate is 
               updated_lr = old_lr / lr_decay. default value is 3. (float)

    h5_tag - A string tagged onto the end of the model file indicating the level of
             regularization and number of filters used. This is helpful for facilitating
             the selection of the best model in a given instance within the ensemble. 
    
    save_intermediate_models - This is an option for testing purposes that, instead
                               only saving the best-performing model, will save the
                               current state of the model after each
                               check_val_every_n_epochs epochs. (Boolean)

    dir_name - This is the directory where the model ensemble will be saved. You
               will need to provide this to cnn.build_cnns() and during calibration
               and modeling scoring. 
    
    sample_size - this is basically only used for doing saturation anaylses,
                  but you can choose to train on only <sample_size> training
                  examples if you want. This doesn't affect how many examples
                  are used from the tune partition.
    """

    assert input_alleles is not None, 'must provide an allele to build model for...'

    # handling command-line arguments, they are strings by default
    if l2_factor is not None:
        if type(l2_factor) == str:
            l2_factor = float(l2_factor)
    
    if input_alleles is not None:
        if type(input_alleles) == str:
            input_alleles = [input_alleles]

    if sample_size is not None:
        if type(sample_size) == str:
            sample_size = int(float(sample_size))

    # these feature selections represent what was done in the Immunity publication
    feat_picks = ['PDB_singlet', 'amino_groups', 'missing_char',
                  'pep_length']
    fix_seed = False

    if (kernel_string is None) or (kernel_string == 'None'):
        kernel_string = '6-6'
    if (filter_string is None) or (filter_string == 'None'):
        filter_string = '50-50'
    if (base_lr is None) or (base_lr == 'None'):
        base_lr = 0.003
    train_decoy_factor = 40
    binder_weight = train_decoy_factor

    # this is a vestige of how code was run outside of this project
    for allele in input_alleles:

        train_all_alleles(kernel_string=kernel_string,
                          binder_weight=binder_weight, filter_string=filter_string,
                          data_dir=data_dir, input_alleles=[allele],
                          model_tag=model_tag, max_bad_epochs=max_bad_epochs,
                          check_val_every_n_epochs=check_val_every_n_epochs,
                          max_ppv_lowering_epochs=max_ppv_lowering_epochs,
                          train_decoy_factor=train_decoy_factor, feat_picks=feat_picks,
                          max_length=max_length, base_lr=base_lr,
                          l2_factor=l2_factor,
                          use_dropout=use_dropout, tune_decoy_factor=tune_decoy_factor,
                          fix_seed=fix_seed, dropout_rate=dropout_rate,
                          lr_decay=lr_decay,
                          h5_tag=h5_tag,
                          save_intermediate_models=save_intermediate_models,
                          dir_name=dir_name,
                          sample_size=sample_size)

    return 


def train_all_alleles(kernel_string=None, sym_short=True, filter_string=None,
                      input_alleles=None, feat_picks=None, model_tag='',
                      data_dir=None, check_val_every_n_epochs=5,
                      max_bad_epochs=3, max_ppv_lowering_epochs=1,
                      l2_factor=None, train_decoy_factor=20,
                      max_length=20, binder_weight=20, base_lr=0.001,
                      padding_choice='valid',use_dropout=True,
                      tune_decoy_factor=20, fix_seed=True, dropout_rate=0.2,
                      lr_decay=3, updated=False, arb_dir=None,
                      h5_tag=None, n_epochs=None, fit_epochs=1,
                      save_intermediate_models=False,
                      dir_name=None, sample_size=None):
    """
    This code interprets the various input paramters, sets up the directory
    to place all model-related content, performs featurization and feature
    normalization for the train/tune partitions for this allele and submits
    them for model training.
    """

    if type(input_alleles) == list:
        allele_string = input_alleles[0]
    else:
        allele_string = input_alleles

    # this shouldn't matter since early stopping will certainly kick in by then.
    n_epochs = 300
    
    # this block of code generates a string to tag our model directory with. This
    # provides some information about the parameters used to train the model.
    dirtag = kernel_string
    if kernel_string is None:
        dirtag = ''
    if max_length != 20:
        dirtag += '_maxlength%d' % (max_length)
    if filter_string is not None:
        dirtag += '_' + filter_string
    if model_tag != '':
        dirtag = model_tag + dirtag
    dirtag += '_%s' %  padding_choice
    if dropout_rate != 0.2:
        if use_dropout:
            dirtag += '_drrate%0.2f' % dropout_rate
    if fix_seed == False:
        dirtag += '_randomseed'
    if max_ppv_lowering_epochs != 1:
        dirtag += '_%d_patience' % max_ppv_lowering_epochs
    if train_decoy_factor != 20:
        dirtag += '_decoy_factor_%d' % train_decoy_factor
    # we are using gpool always
    dirtag += '_gpool'
    if sample_size is not None:
        dirtag += '_%dpts' % sample_size

    # converts the kernel string into a list of kernel sizes. 
    if kernel_string is not None:
        if '-' in kernel_string:
            if kernel_string[-1] == '-':
                kernels = [int(kernel_string.split('-')[0])]
            else:
                kernels = [int(x) for x in kernel_string.split('-')]
        else:
            kernels = [int(x) for x in list(kernel_string)]
        motif_win = sum(kernels) - len(kernels) + 1
    else:
        kernels = None

    # converts filter string into a list of filter counts.
    if filter_string is not None:
        if '-' in filter_string:
            filters = [int(x) for x in filter_string.split('-')]
        else:
            filters = [int(x) for x in list(filter_string)]
    else:
        filters = None

    if feat_picks is None:
        feat_picks = ['blosum62', 'pep_length', 'missing_char']

    # ok we also need to know the set of features to use.
    feat_dict = processing.get_default_feature_selection_dict()
    for key in list(feat_dict.keys()):
        if key in feat_picks:
            feat_dict[key] = True
            dirtag += '_' + key
        else:
            feat_dict[key] = False

    if not os.path.exists(dir_name):
        os.makedirs(dir_name, exist_ok=True)
    if dirtag != '':
        dir_name += dirtag + '/'
    if not os.path.exists(dir_name):
        os.makedirs(dir_name, exist_ok=True)
    fd_name = dir_name + 'feature_selection.yaml'
    with open(fd_name, 'w') as f:
        yaml.dump(feat_dict, f, default_flow_style=False)
        
    # in practice, there will only be one allele. this is another vestige
    # of the code being used for for purposes outside the publication
    for allele in input_alleles:
        print('\n\nBeginning Featurization for %s Model...\n\n' % allele)
        # this code generates the training data for the specified allele.
        train_data, train_labels, train_weights, df_train =\
            gc.get_training_data(max_length=max_length, allele=allele,
                                 feat_change_dict=feat_dict, sym_short=sym_short,
                                 data_dir=data_dir,
                                 partition='train', decoy_factor=train_decoy_factor,
                                 fix_seed=fix_seed,
                                 sample_size=sample_size)

        # generate validation data for the specified allele
        val_data, val_labels, val_weights, df_val =\
            gc.get_binders_with_shuffling(allele=allele, max_length=max_length,
                                          feat_change_dict=feat_dict, sym_short=sym_short,
                                          data_dir=data_dir,
                                          decoy_factor=tune_decoy_factor, fix_seed=fix_seed)
                                          

        # perform feature normalization based on the feature value distributions
        # in the training partition. This also saves the normalization
        # parameters inside of the model directory for use when applying
        # to new examples.
        train_data, val_data = norm_inputs(train_array=train_data,
                                           test_array=val_data,
                                           base_dir=dir_name,
                                           allele=allele)

        # let's add in the alleles if they aren't in there yet
        if 'allele' not in list(df_train.columns.values):
            df_train['allele'] = allele
        if 'allele' not in list(df_val.columns.values):
            df_val['allele'] = allele

        # this does the heavy lifting of the actual model training.
        print('\nBeginning Model Building for %s...\n' % allele)
        model, ppv = prelim_build(train_data=train_data, train_labels=train_labels,
                                  train_weights=train_weights, val_data=val_data,
                                  val_labels=val_labels, val_weights=val_weights,
                                  df_val=df_val, n_epochs=n_epochs,
                                  check_val_every_n_epochs=check_val_every_n_epochs,
                                  binder_weight=binder_weight, model=None,
                                  save_intermediate_models=save_intermediate_models,
                                  df_train=df_train, loss_fun='binary_crossentropy',
                                  fbase=dir_name, allele=allele,
                                  kernels=kernels, filters=filters,
                                  max_bad_epochs=max_bad_epochs,
                                  max_ppv_lowering_epochs=max_ppv_lowering_epochs,
                                  l2_factor=l2_factor, max_length=max_length,
                                  base_lr=base_lr, padding_choice=padding_choice,
                                  use_dropout=use_dropout, fix_seed=fix_seed,
                                  dropout_rate=dropout_rate,
                                  lr_decay=lr_decay, h5_tag=h5_tag, fit_epochs=fit_epochs)

                    
    return 


def prelim_build(model=None, train_data=None, train_labels=None, train_weights=None,
                 df_train=None, val_data=None, val_labels=None, val_weights=None,
                 df_val=None, do_validation=True, n_epochs=11,
                 check_val_every_n_epochs=10, allele='DRB1_1104',
                 binder_weight=20, loss_fun='binary_crossentropy',
                 base_lr=0.001, save_intermediate_models=False, fbase='',
                 model_file_tag='', kernels=None, filters=None, max_length=20,
                 max_bad_epochs=3, max_ppv_lowering_epochs=1, l2_factor=None,
                 padding_choice='valid', use_dropout=True, fix_seed=True,
                 dropout_rate=0.2, lr_decay=3, h5_tag=None, fit_epochs=1):
    """
    This function does the model training, early stopping, model serialization
    """
    # fix random number seeds
    if fix_seed:
        np.random.seed(666)
        set_random_seed(667)
    else:
        np.random.seed()
        
    if h5_tag is not None:
        # this is a tag we'll append to the end of the MODEL FILES rather
        # than the model directory. We want to make sure it doesn't
        # iterfere with looking models up further down the line
        if h5_tag[0] != '_':
            h5_tag = '_' + h5_tag
        
    # training parameters
    batch_size = 256
    
    # the goal of the block of code below is to ensured that the summed weights
    # for the hit training examples equals the summed weights for the decoy
    # training examples. The examples' weights have already been modified by
    # this point to have each nested set have equal weight.
    print('manually adjusting sample weights to reflect class weights...')
    tw = np.copy(train_weights)
    tw[train_labels == 1] = binder_weight*tw[train_labels == 1]
    tw[train_labels == 0] = 1.0*tw[train_labels == 0]

    # initalize the CNN
    num_feats = train_data.shape[-1]
    input_shape = (max_length, num_feats)
    model, loss_fun, base_lr =\
        model_initializer(input_shape=input_shape, base_lr=base_lr,
                          kernels=kernels, filters=filters,
                          l2_factor=l2_factor,
                          padding_choice=padding_choice, use_dropout=use_dropout,
                          loss_fun=loss_fun,
                          dropout_rate=dropout_rate)

    # just a few variables to track progress while training
    last_loss = 100
    bad_epochs = 0
    lowest_loss = last_loss
    stop_condition = False
    prior_val_ppv = 0
    final_max_ppv = 0
    n_ppv_lowering_epochs = 0
    for i in range(n_epochs):
        n_outputs = 1

        # train history is an object we can inspect to look at
        # loss/metric values corresponding to this epoch
        train_history = model.fit(train_data, [train_labels]*n_outputs, epochs=fit_epochs,
                                  batch_size=batch_size, verbose=1, shuffle=True,
                                  sample_weight=tw)

        # look at the current training loss, compare to historical min, calculate
        # percent change.
        current_loss = train_history.history['loss'][0]
        percent_change = 100.0*(current_loss - last_loss)/(.0001 + lowest_loss)
        last_loss = current_loss
        if current_loss < lowest_loss:
            lowest_loss = current_loss
        
        # check if we want to lower the learning rate
        if percent_change > -1:
            base_lr = base_lr / lr_decay
            print('learning has slowed, new (lowered) learning rate:', base_lr)
            adam = optimizers.Adam(lr=base_lr)
            model.compile(loss=loss_fun, optimizer=adam)
            # increment the bad epochs count
            bad_epochs += 1
            if percent_change > 0:
                # if the training loss _increased_, double-count the penalty.
                bad_epochs += 1
        else:
            # if we have a sufficently-large decrease in the training loss,
            # reset the running count of "bad epochs".
            bad_epochs = 0
        print('percent change in loss from last epoch:', percent_change)

        # every few epochs, we re-check the validation PPV,
        # this can be a time-consuming step for larger models. 
        if (i % check_val_every_n_epochs == 0) & (i != 0) & do_validation:
            print('\tinspecting validation performance for epoch', i+1)
            y_pred = model.predict(val_data)
            df_val['prediction'] = y_pred

            val_preds = df_val['prediction'].values
            val_labels = df_val['label'].values

            # print this epoch's ppv
            epoch_ppv = top_ppv(val_labels, val_preds)
            
            # this is a sanity check that has proven to be useful at times.
            # we check to see what fraction of the scored peptides have share
            # the maximum predicted value. if this is common, it usually
            # signals that there is a bug somewhere.
            max_pred = df_val['prediction'].max()
            n_max = df_val.loc[df_val['prediction'] == max_pred].shape[0]
            n_hits = df_val['label'].sum()
            if n_max > n_hits:
                print('\n YOU HAVE DEGENERACY IN YOUR SCORING \n')
                epoch_ppv = \
                    df_val.loc[df_val['prediction'] == max_pred]['label'].mean()
            # present confusion matrix.
            give_confusion_matrix(val_labels, val_preds)
            print('Estimated PPV for this epoch is:', epoch_ppv)

            # here we store the predictions on the validation set. Can be useful for
            # inspecting/debugging.
            df_val.to_csv(fbase + 'val_scores_' + allele + '_epoch_'
                          + str(i+1) + '.csv', sep=',', index=False)

            # we want to keep a running tally of the highest PPV we attain on the
            # tune partition, keep track of the corresponding model. 
            if epoch_ppv > final_max_ppv:
                final_max_ppv = epoch_ppv
                best_model = keras.models.clone_model(model)
                adam = optimizers.Adam(lr=base_lr)
                model.compile(loss=loss_fun, optimizer=adam)
                best_model.set_weights(model.get_weights())
                best_model_filename = \
                    '%s_best_model_PPVval_%f%s.h5' % (allele, epoch_ppv, h5_tag)

            # also for the training data
            y_pred_train = model.predict(train_data)

            epoch_train_ppv = top_ppv(train_labels, y_pred_train)
            print('Estimated TRAIN PPV for this epoch is:', epoch_train_ppv)
            print('TRAIN CONFUSION MATRIX')
            give_confusion_matrix(train_labels, y_pred_train)

            # print out score deciles, this could be helpful for debugging
            percentiles = np.linspace(10, 100, 10)
            train_cdf = np.percentile(y_pred_train, percentiles)
            val_cdf = np.percentile(y_pred, percentiles)
            print('Validation Score Deciles\n', val_cdf)
            print('Train Score Deciles\n', train_cdf)

            if save_intermediate_models:
                # for testing purposes, we may want to save intermediate
                # versions of the model.
                round_ppv_val = np.round(epoch_ppv, 3)
                round_ppv_train = np.round(epoch_train_ppv, 3)
                model_filename = \
                    '%s_epoch_%d_PPVval_%f_PPVtrain_%f.h5' % (allele, i+1,
                                                              round_ppv_val,
                                                              round_ppv_train)
                if h5_tag is not None:
                    model_filename = model_filename.replace('.h5', h5_tag+'.h5')
                if model_file_tag != '':
                    model_filename = model_file_tag + '_' + model_filename
                model.save(fbase+model_filename)
            
            if epoch_ppv < prior_val_ppv:
                # allow for more than one check where our validation ppv
                # is lower than the historical max...
                if epoch_ppv < final_max_ppv:
                    n_ppv_lowering_epochs += 1
                    if n_ppv_lowering_epochs >= max_ppv_lowering_epochs:
                        print('\tPPV on validation set has gone down, stopping training...')
                        stop_condition = True

            prior_val_ppv = epoch_ppv

        # if we haven't made progress with the training loss
        # or if the validation PPV values went down, stop training..
        if (bad_epochs > max_bad_epochs) or stop_condition:
            val_ppv, train_ppv = \
                inspect_validation_performance(train_data=train_data,
                                               train_labels=train_labels,
                                               val_data=val_data,
                                               df_val=df_val,
                                               model=model,
                                               n_outputs=n_outputs,
                                               multi_i=1, save_dir=fbase,
                                               allele=allele, epoch_count=(i+1),
                                               allele_string=allele)
            
            if val_ppv > final_max_ppv:
                # create a copy of the model when it had its highest PPVval
                final_max_ppv = val_ppv
                best_model = keras.models.clone_model(model)
                adam = optimizers.Adam(lr=base_lr)
                model.compile(loss=loss_fun, optimizer=adam)
                best_model.set_weights(model.get_weights())
                best_model_filename = \
                    '%s_best_model_PPVval_%f%s.h5' % (allele, val_ppv, h5_tag)
                
            if save_intermediate_models:
                # again, for testing purposes it might be helpful to save
                # models throughout the training.
                round_ppv_val = np.round(val_ppv, 3)
                round_ppv_train = np.round(train_ppv, 3)
                model_filename = \
                    '%s_epoch_%d_PPVval_%f_PPVtrain_%f.h5' % (allele,
                                                              i+1,
                                                              round_ppv_val,
                                                              round_ppv_train)
                if h5_tag is not None:
                    model_filename = model_filename.replace('.h5', h5_tag+'.h5')
                if model_file_tag != '':
                    model_filename = model_file_tag + '_' + model_filename

                model.save(fbase+model_filename)
                max_ppv = np.max((val_ppv, prior_val_ppv))

            # save the best model...
            best_model.save(fbase + best_model_filename)

                
            return model, final_max_ppv

    return model, final_max_ppv


def model_initializer(input_shape=None, base_lr=0.001, kernels=None, filters=None,
                      l2_factor=None, leaky_relu=False, merge_style='concatenate',
                      padding_choice='valid', use_dropout=True, gpool=True,
                      loss_fun=None, dropout_rate=0.2, do_sgd=True, amsgrad=False):
    """
    This is a method that will initialize our model given the desired number
    of filters and channels per string and learning rate, dropout rate, etc.
    """


    if kernels is None:
        kernels = [6, 6]
        
    if filters is None:
        n_feature_maps = [50, 50]
    else:
        n_feature_maps = filters
        
    if merge_style == 'add':
        input_dim = input_shape[1]
        n_feature_maps = [input_dim, input_dim,
                          input_dim, input_dim,
                          input_dim, input_dim]
        
        
    l2s = [0.05, 0.1, 0, 0, 0, 0, 0, 0, 0, 0.01]
    if kernels[0] == 1:
        l2s = [0] + l2s
        
    if l2_factor != None:
        l2s = [l2_factor*x for x in l2s]
    
    if leaky_relu:
        activations = ['linear', 'linear', 'linear', 'linear', 'sigmoid']
    else:
        activations = ['relu', 'relu', 'relu', 'relu',
                       'relu', 'relu', 'relu', 'relu',
                       'relu', 'relu', 'relu', 'relu',
                       'relu', 'relu', 'relu', 'relu',
                       'sigmoid']
    
    use_batchnorm = True

    if do_sgd:
        adam = optimizers.SGD(lr=base_lr)
    else:
        rmsp = optimizers.RMSprop(decay=0.01)
        adam = optimizers.Adam(lr=base_lr, amsgrad=amsgrad)
    if loss_fun is None:
        loss_fun = 'binary_crossentropy'

    # this is the optimal initializer when using relu activation functions
    vs_init = initializers.he_normal(seed=666)
    
    inputs = keras.layers.Input(shape=input_shape)
    n_conv_layers = len(kernels)
    for i in range(n_conv_layers):
        # ok, first perform the convolution, batch norm, dropout
        if i == 0:
            x = keras.layers.Conv1D(kernel_size=(kernels[i]), filters=n_feature_maps[i],
                                    activation=activations[i],
                                    kernel_regularizer=regularizers.l2(l2s[i]),
                                    kernel_initializer=vs_init, padding=padding_choice)(inputs)
        else:
            x = keras.layers.Conv1D(kernel_size=(kernels[i]), filters=n_feature_maps[i],
                                    activation=activations[i],
                                    kernel_regularizer=regularizers.l2(l2s[i]),
                                    kernel_initializer=vs_init, padding=padding_choice)(x)
        if leaky_relu:
            x = keras.layers.LeakyReLU(alpha=0.100)(x)
        if use_batchnorm:
            x = keras.layers.BatchNormalization()(x)
        if use_dropout:
            x = keras.layers.SpatialDropout1D(dropout_rate)(x)
        
        # then calculate maxpool, concatenate avg pool
        if i == 0:
            all_max_pools = keras.layers.GlobalMaxPooling1D()(x)
            if gpool:
                all_avg_pools = keras.layers.GlobalAveragePooling1D()(x)
                all_max_pools = keras.layers.concatenate([all_max_pools, all_avg_pools])
        else:
            max_pool = keras.layers.GlobalMaxPooling1D()(x)
            if gpool:
                avg_pool = keras.layers.GlobalAveragePooling1D()(x)
                max_pool = keras.layers.concatenate([max_pool, avg_pool])
        # ok, if it's not the first time, we append max pool
        if i > 0:
            all_max_pools = keras.layers.concatenate([all_max_pools, max_pool])
            
    predictions = keras.layers.Dense(1, activation=activations[-1],
                                     kernel_regularizer=regularizers.l2(l2s[-1]))(all_max_pools)
    model = keras.models.Model(inputs=inputs, outputs=predictions)

    model.compile(optimizer=adam, loss=loss_fun)
    
        
    return model, loss_fun, base_lr



def inspect_validation_performance(train_data=None, train_labels=None,
                                   val_data=None, df_val=None, model=None,
                                   n_outputs=1, multi_i=None, save_dir=None,
                                   allele='', epoch_count=None, allele_string=None):
    if allele_string is None:
        allele_string = allele
    y_pred = model.predict(val_data)

    if (n_outputs > 1):
        y_pred = y_pred[-1]
    df_val['prediction'] = y_pred

    group_preds = df_val['prediction'].values
    group_labels = df_val['label'].values

    # print this epoch's ppv
    epoch_ppv = top_ppv(group_labels, group_preds)

    max_pred = df_val['prediction'].max()
    n_max = df_val.loc[df_val['prediction'] == max_pred].shape[0]
    n_hits = df_val['label'].sum()
    if n_max > n_hits:
        print('\n THERE IS DEGENERACY IN YOUR SCORING \n')
        epoch_ppv = df_val.loc[df_val['prediction'] == max_pred]['label'].mean()
    print('Estimated PPV for this epoch is:', epoch_ppv)
    
    # present confusion matrix
    give_confusion_matrix(group_labels, group_preds)

    # we should also do this for the training data
    y_pred_train = model.predict(train_data)
    y_pred_train = y_pred_train[-1]
    if np.shape(np.shape(y_pred_train))[0] > 1:
        y_pred_train = y_pred_train[:, 0]

    epoch_train_ppv = top_ppv(train_labels, y_pred_train)
    print('Estimated TRAIN PPV for this epoch is:', epoch_train_ppv)

    if save_dir is not None:
        df_val.to_csv(save_dir + 'val_scores_' + allele_string + '_' +\
                      str(epoch_count) + '.csv', sep=',', index=False)
    
    return epoch_ppv, epoch_train_ppv


def top_ppv(y_true, y_pred, n_pos=None):
    """
    Return PPV for a given set of predictions. By default,
    look at the number of examples that are positive and assigns
    that to n_pos. Then sort all predictions and takes the largest
    n_pos of them. The fraction that are correct is the PPV.
    """
    
    if np.shape(np.shape(y_pred))[0] > 1:
        y_pred = y_pred[:, 0]
    if n_pos is None:
        n_pos = int(y_true.sum())
    sortidx = np.argsort(y_pred)

    return y_true[sortidx][-n_pos:].mean()    


def give_confusion_matrix(y_true, y_pred):
    """
    This should just threshold predictions at the appropriate value
    and then print a corresponding confusion matrix.
    """

    percentile_threshold = 100.0 * (1 - np.mean(y_true))
    score_threshold = np.percentile(y_pred, percentile_threshold)
    binned_scores = np.zeros(np.shape(y_pred))
    binned_scores[y_pred >= score_threshold] = 1
    cm = confusion_matrix(y_true, binned_scores)
    print('confusion matrix:')
    print(cm)

    return 


def norm_inputs(train_array=None, test_array=None, base_dir='', allele=''):
    num_feats = np.shape(train_array)[-1]
    feat_number = []
    feat_mean = []
    feat_std = []
    for i in range(num_feats):
        feat_number += [i]
        raveled_feat = np.ravel(train_array[:, :, i])
        the_mean = np.mean(raveled_feat)
        feat_mean += [the_mean]
        the_stdd = np.sqrt(np.var(raveled_feat))
        feat_std += [the_stdd]
        train_array[:, :, i] = (train_array[:, :, i] - the_mean) / (the_stdd + .00001)
        test_array[:, :, i] = (test_array[:, :, i] - the_mean) / (the_stdd + .00001)

    # create dataframe with means and standard deviations, save to disk
    df = pd.DataFrame({'feat_number': feat_number,
                       'feat_mean': feat_mean,
                       'feat_stdd': feat_std})
    if base_dir != '':
        if base_dir[-1] != '/':
            base_dir += '/'

    full_filename = base_dir + allele + '_feat_normalization_params.csv'
    if len(full_filename) > 350:
        full_filename = base_dir + 'feat_normalization_params.csv'
    df[['feat_number', 'feat_mean', 'feat_stdd']].to_csv(full_filename, sep=',', index=False)
        
    return train_array, test_array


if __name__ == "__main__":
    train_wrapper()
