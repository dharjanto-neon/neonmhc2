import os
import click
import glob
import numpy as np
import pandas as pd
import sys
import pdb

from sklearn.isotonic import IsotonicRegression

import setup_code
import processing


@click.command()
@click.option('--outfile')
@click.option('--ptile_lookup')
@click.option('--scan_mode')
@click.option('--infile')
@click.option('--allele')
@click.option('--no_header')
@click.option('--model_base_dir')
@click.option('--model_string')
def score_wrapper(outfile=None, ptile_lookup=True, invert_ptile=True, scan_mode=False,
                  infile=None, allele=None, no_header=False, model_base_dir=None,
                  model_string=None):
    """
    INPUTS
    outfile - This is the name of the file that the resulting scores will be written to

    ptile_lookup - This is a boolean indicating if we are calculating score percentiles
                   based on the 100k reference peptides. In production, this is what
                   we should do.

    infile - Path to an input file with peptides to be scored.

    scan_mode - Boolean This applies a 12-20mer
                scanning across the input peptides. This is relevant when you are
                scoring peptides for a binding affinity assay or something like that.

    outfile - Desired output path. If infile is not None and
              outfile is None, then this should default to scored_<infile>

    no_header - Boolean indicating whether
                the input file has a header. If no_header is True, we assume infile
                is a newline-separated list of peptides.

    model_base_dir - This is the directory where your model ensemble lives. This is
                     the exact same path as "dir_name" that was used by cnn.py
                     when building the model.

    model_string - (Optional) In addition to model_base_dir, this specifies the
                   model that you are using to score the peptides in infile.
                   If you built the models with the default parameters then you
                   do not need to provide this.

    EXAMPLE USAGE:

    > python3 neonmhc2.py --model_base_dir /path/to/model/
      --infile peptides_with_peptide_column_header.csv --allele DRB1_0101
      --outfile output_file.csv 

    > python3 neonmhc2.py --infile sample_peptides_with_alleles_and_headers.csv 
      --model_base_dir /path/to/model/

    > python3 neonmhc2.py --infile list_of_peptides_with_no_header.txt
      --allele DRB3_0101 --no_header True
      --outfile name_of_desired_output_file.txt 
      --model_base_dir /path/to/model/
    """

    # parse inputs, which have string types by default
    if type(ptile_lookup) == str:
        if ptile_lookup == 'True':
            ptile_lookup = True
        else:
            ptile_lookup = False

    if type(scan_mode) == str:
        if scan_mode == 'True':
            scan_mode = True
        else:
            scan_mode = False

    if type(no_header) == str:
        if no_header == 'True':
            no_header = True
        else:
            no_header = False
            
            
    # this means we are applying the model to some input peptides and not
    # part of the recon pipeline
    score_peptides_cli(infile=infile, outfile=outfile, allele=allele,
                           no_header=no_header, save_bool=True,
                           scan_mode=scan_mode, model_base_dir=model_base_dir,
                           model_string=model_string)
    
    return


def score_peptides_cli(infile=None, outfile=None, allele=None, no_header=False,
                       save_bool=True, df_in=None, ptile_lookup=True,
                       invert_ptile=True, scan_mode=False,
                       model_base_dir=None, model_string=None, data_dir=None):
    """
    This function involves a few bookkeeping checks and then calls a general
    function for scoring input peptides. 
    """

    addl_cols = ['allele', 'peptide', 'mean_percentile']
    
    # specify the set of models that we want to use
    assert model_base_dir is not None, 'please provide model directory...'

    if model_string is None:
        # this describes the type of model that was built. If you manually edit the
        # model building code, then you will need to pass a different model string
        # to this function. This is the one that will be valid by default
        model_string = '6-6_50-50_valid_randomseed_3_patience_decoy_factor_' +\
                       '40_gpool_PDB_singlet_amino_groups_missing_char_pep_length'

    if df_in is None:
        if infile[-4::] in ['.tab', '.tsv', '.txt']:
            delim = '\t'
        else:
            delim = ','

        if no_header != False:
            if no_header == 'True':
                no_header = True
            
        if no_header:
            df_pep = pd.read_csv(infile, sep=delim, names=['peptide'])
        else:
            df_pep = pd.read_csv(infile, sep=delim)
    else:
        df_pep = df_in

    # validate that peptides are in the right format...
    df_pep = validate_peptides(df_in=df_pep)

    # window across with 12-20mers if scan_mode
    if scan_mode:
        df_pep = setup_code.scan_across_peps(df_in=df_pep, l_min=12, l_max=20)
        del df_pep['group_id']
        
    slice_df = False
    if allele is None:
        slice_df = True
        print('since no allele was provided when issuing the command, we will assume that')
        print('the input file has an allele column indicating which allele to' +\
              'score each peptide with...')
        assert 'allele' in list(df_pep.columns.values), 'please provide a column ' +\
            'indicating allele for each peptide...'
        alleles = list(df_pep['allele'].unique())
    else:
        assert '_' in allele, 'please provide allele in format like "DRB1_0101"...'
        assert ':' not in allele, 'please provide allele in format like "DRB1_0101"...'
        assert '.' not in allele, 'please provide allele in format like "DRB1_0101"...'
        assert '*' not in allele, 'please provide allele in format like "DRB1_0101"...'
        alleles = [allele]

        if 'allele' in list(df_pep.columns.values):
            print('\nrenaming your allele column to original_allele...\n')
            df_pep.rename(columns={'allele': 'original_allele'}, inplace=True)
            
            
    # ok ok ok next we want to validate that the chosen alleles are ok...
    good_alleles = setup_code.get_latest_allele_list(model_base_dir=model_base_dir,
                                                     model_string=model_string)
    bad_alleles = [x for x in alleles if x not in good_alleles]
    if len(bad_alleles) > 0:
        print('\n\nWe are skipping the following unsupported alleles:')
        for bad_allele in bad_alleles:
            print('\t%s...' % bad_allele)
        print('\n\n')
            
    alleles = [x for x in alleles if x in good_alleles]
    
    is_first = True
    for allele in alleles:
        df_allele = df_pep.copy()
        if slice_df:
            df_allele = df_allele.loc[df_allele['allele'] == allele]
        else:
            df_allele['allele'] = allele
            
        # prevent crash if only one peptide is being scored
        if df_allele.shape[0] == 1:
            df_allele = df_allele.append(df_allele)
            
        df_allele = generic_scoring_prank(df_pep=df_allele, allele=allele,
                                          model_base_dir=model_base_dir,
                                          model_string=model_string,
                                          ptile_lookup=ptile_lookup,
                                          invert_ptile=invert_ptile)
        
        # if scan mode is on, we want to select the top-scoring 12-20mer
        # per peptide. sort direction depends on if high percentiles are good
        if scan_mode:
            df_allele = df_allele.sort_values('mean_percentile',
                                              ascending=invert_ptile)
            df_allele = df_allele.drop_duplicates(subset=['allele', 'original_peptide'],
                                                  keep='first')
            
        df_allele = df_allele.drop_duplicates(subset=['allele', 'peptide'])
        
        if is_first:
            df_out = df_allele
            is_first = False
        else:
            df_out = df_out.append(df_allele)
            
    original_cols = list(df_pep.columns.values)
    output_cols = list(set(original_cols + addl_cols))
    output_cols = [x for x in output_cols if x in list(df_out)]    
    
    if (outfile is None) and (save_bool):
        base_file = infile.split('/')[-1]
        
        if scan_mode:
            outfile = infile.replace(base_file, 'scored_scan_' + base_file)
        else:
            outfile = infile.replace(base_file, 'scored_' + base_file)
            
    output_cols = [x for x in output_cols if x in list(df_out.columns.values)]
    if save_bool:
        print('\nSAVING OUTPUT HERE: %s...' % outfile)
        df_out[output_cols].to_csv(outfile, sep=delim, index=False)

        
    return df_out[output_cols]
    

def generic_scoring_prank(df_pep=None, allele='DRB1_0101', model_base_dir=None,
                          model_string=None, n_models=10,
                          pep_col='peptide', sym_short=True, max_length=20,
                          data_dir=None, save_apply=False, ptile_lookup=True,
                          invert_ptile=True):
    """
    This is the method we will use to score an input set of peptides for a particular allele.

    ptile_lookup - This is a relatively important input basically indicating if we are
                   running an apply-time scoring or a calibration scoring. In the
                   case of a calibration scoring, we will assume that we are scoring the
                   100k reference peptides. We use the scores on the 100k peptides
                   to generate a score <-> percentile
                   mapping. 
                   False - This implies calibration
                   True - This implies we are doing a real scoring. This relies on a calibration
                          scoring having been done since the models were built.

    save_apply - If you are doing a calibration run, then you need to have save_apply = True.
                 This will save the results of the score <-> percentile mapping on the 100k
                 reference peptides. You can use this code with ptile_lookup=False and
                 save_apply=False if you want to do scoring for fun but haven't yet calibrated.

    df_pep - This is a dataframe that has a column with the peptides that
             we want to score. The column is <pep_col>

    allele - We will load an ensemble of mono-allelic models, but for which allele?

    model_base_dir - Each model in the ensemble has its own directory, but
                     this indicates the directory that all of these models are stored in.
                     This should be equal to the dir_name used when you train the model

    model_string - Indicates the part of the directory name that describes the model
                   but excludes that "iteration_n_of_N" part. If you haven't edited
                   the model building code, you should not need to adjust this

    n_models - number of models in the ensemble that we are going to use to score.
               You must score with all the models in your ensemble, if you trained 10
               iterations, this must be 10. 10 is default and you shouldn't need to adjust.
    
    invert_ptile - Boolean. If true, low percentile scores indicate likely binding.
                   Public predictor has low percentages indicating high binding likelihood,
                   so that is the default here.
    """


    if model_string is None:
        # this describes the type of model that was built. If you manually edit the
        # model building code, then you will need to pass a different model string
        # to this function. This is the one that will be valid by default
        model_string = '6-6_50-50_valid_randomseed_3_patience_decoy_factor_' +\
                       '40_gpool_PDB_singlet_amino_groups_missing_char_pep_length'
            
    print('loading ensemble of models for %s allele...' % allele)
    models, feat_dicts, model_dirs = \
        setup_code.get_ensemble_model_list(allele=allele, n_models=n_models,
                                           base_dir=model_base_dir, model_string=model_string)

    print('assuming featurizations are consistent between models...')
    feat_dict = feat_dicts[0]
    print('also, assuming you have provided a dataframe with one row per peptide')
    print('to be scored and that the peptide is in the %s column...' % pep_col)
    addl_annoying_cols = []
    col_names = list(df_pep.columns.values)
    if 'weights' not in col_names:
        df_pep['weights'] = 1
        addl_annoying_cols += ['weights']
    if 'label' not in col_names:
        df_pep['label'] = -1
        addl_annoying_cols += ['label']
    if 'group_id' not in col_names:
        df_pep['group_id'] = df_pep.index
        addl_annoying_cols += ['group_id']
    print('featurizing dataframes...')
    if ptile_lookup:
        print('NOT featurizing tune dataframe, looking up scores from reference peptides...')
    else:
        print('\ttune dataframe...')
        val_data, val_labels, val_weights, df_val = \
            setup_code.get_binders_with_shuffling(allele=allele, max_length=max_length,
                                                  feat_change_dict=feat_dict, sym_short=sym_short,
                                                  data_dir=data_dir)

    print('\tapply dataframe...')
    apply_data, apply_labels, apply_weights, df_apply = \
        processing.featurize_dataframe(df_pep=df_pep, max_length=max_length,
                                       feature_change_dict=feat_dict, sym_short=sym_short)
    if ptile_lookup:
        print('NOT featurizing a hits dataframe, relying on saved secondary alignment info...')
    else:
        print('\thit dataframe...')
        df_hit = df_val.loc[df_val['label'] == 1]
        hit_data, hit_labels, hit_weights, df_hit = \
            processing.featurize_dataframe(df_pep=df_hit, max_length=max_length,
                                           feature_change_dict=feat_dict, sym_short=sym_short)
    print('normalizing inputs...')

    apply_data, success_apply = norm_inputs_from_disk(test_array=apply_data,
                                                      base_dir=model_dirs[0],
                                                      allele=allele)
    assert success_apply, 'unable to find feature normalization params...'
    
    if ptile_lookup == False:
        val_data, success_val = norm_inputs_from_disk(test_array=val_data,
                                                      base_dir=model_dirs[0],
                                                      allele=allele)
        hit_data, success_hit = norm_inputs_from_disk(test_array=hit_data,
                                                      base_dir=model_dirs[0],
                                                      allele=allele)
        assert success_val, 'unable to find feature normalizatin params...'

    assert len(models) == n_models, 'number of models found does not match input...'
    
    for i in range(n_models):
        print('\n\tAPPLYING MODEL %d of %d...\n' % (i+1, n_models))
        model = models[i]
        model_dir = model_dirs[i]
        if model_dir[-1] != '/':
            model_dir += '/'

        filetag = '%s_%d' % (allele, (i+1))
        
        if ptile_lookup:
            # load reference scores to look up score <-> percentile relationship
            lookup_file = model_dir + 'best_model_reference_percentiles_%s.csv' % allele
            df_lookup = pd.read_csv(lookup_file, sep=',')
            # just to be sure
            df_lookup = df_lookup.sort_values('percentile')
            percentiles = df_lookup['percentile'].values
            score_cdf = df_lookup['prediction'].values
            prob_cdf = df_lookup['probability'].values

            predictions = model.predict(apply_data)

            pred_col = 'predictions_%d' % (i+1)
            perc_col = 'percentiles_%d' % (i+1)
            prob_col = 'probability_%d' % (i+1)

            # ok this is a column containing raw predictions
            df_apply[pred_col] = predictions
            # take those raw predictions, look them up based on score cdf,
            # look for corresponding %-tile
            df_apply[perc_col] = \
                df_apply[pred_col].apply(lambda x: np.interp(x, score_cdf, percentiles))
            # take those raw predictions, look them up based on score_cdf,
            # look for correspondign probability
            df_apply[prob_col] = \
                df_apply[pred_col].apply(lambda x: np.interp(x, score_cdf, prob_cdf))
                
        else:
            percentiles = np.linspace(1, 100, 100)
            val_scores = model.predict(val_data)

            # this only needs to be done if we aren't looking up percentiles
            # from the scores on the reference 100k peptides.
            score_cdf = np.percentile(val_scores, percentiles)
            score_threshold = np.percentile(val_scores, 100.0 - 100.0/21.0)
            nbins = int(np.shape(val_scores)[0]/40)
            seg_x, seg_y = do_segmentation(val_scores=val_scores, val_labels=val_labels,
                                           dirtag=model_dir, filetag=filetag)
            val_x, val_y = plot_isotonic_regression(val_scores=seg_x, val_labels=seg_y,
                                                    dirtag=model_dir, filetag=allele)
            sorted_x = [x for _, x in sorted(zip(val_y, val_x))]
            sorted_y = [y for y, _ in sorted(zip(val_y, val_x))]

            predictions = model.predict(apply_data)
            val_predictions = model.predict(val_data)
                
            pred_col = 'predictions_%d' % (i+1)
            perc_col = 'percentiles_%d' % (i+1)
            prob_col = 'probability_%d' % (i+1)
            df_apply[pred_col] = predictions
            df_apply[perc_col] = \
                df_apply[pred_col].apply(lambda x: np.interp(x, score_cdf, percentiles))
            df_apply[prob_col] = \
                df_apply[pred_col].apply(lambda x: np.interp(x, sorted_x, sorted_y))

            df_apply['current_predictions'] = predictions
            df_apply['current_percentiles'] = \
                df_apply['current_predictions'].apply(lambda x: np.interp(x, score_cdf, percentiles))
            df_apply['current_probabilities'] = \
                df_apply['current_predictions'].apply(lambda x: np.interp(x, sorted_x, sorted_y))

            # save apply is only relevant when you ARE NOT using ptile lookup        
            if save_apply:
                # ok let's go ahead and just save the percentiles of the scores,
                # this will take up less space, obviously better than saving all scores
                percentiles = np.linspace(0, 100, 1001)
                score_cdf = np.percentile(df_apply['current_predictions'].values, percentiles)
                prob_cdf = np.percentile(df_apply['current_probabilities'].values, percentiles)
                df_ref = pd.DataFrame(data={'percentile': percentiles,
                                            'prediction': score_cdf,
                                            'probability': prob_cdf})
                save_file = model_dir + 'best_model_reference_percentiles_%s.csv' % allele
                df_ref.to_csv(save_file, sep=',', index=False)
            
        
        hits_col = 'label'
        
        # ok also we want to hang onto the validation scores
        if ptile_lookup == False:
            df_val['predictions_%d' % (i+1)] = val_predictions
            val_ppv = \
                df_val.sort_values('predictions_%d' % (i+1),
                                   ascending=False).head(df_val[hits_col].sum())[hits_col].mean()
        
    # ok, let's make lists of features that we want to construct
    # aggregate features out of
    pred_cols = []
    perc_cols = []
    prob_cols = []
    for i in range(n_models):
        perc_cols += ['percentiles_%d' % (i+1)]
        pred_cols += ['predictions_%d' % (i+1)]
        prob_cols += ['probability_%d' % (i+1)]
        
    df_apply['mean_probability'] = df_apply[prob_cols].mean(axis=1)
    df_apply['mean_percentile'] = df_apply[perc_cols].mean(axis=1)
    df_apply['median_percentile'] = df_apply[perc_cols].median(axis=1)
    df_apply['mean_prediction'] = df_apply[pred_cols].mean(axis=1)

    # default is to have lower percentiles indicate higher binding likelihood,
    # opposite from what was done in development.
    if invert_ptile:
        df_apply['mean_percentile'] = 100 - df_apply['mean_percentile']
    

    # some cleanup
    annoying_cols = ['core', 'first_i', 'pep_len', 'pad', 'backup_pad',
                     'backup_last_i', 'last_i', 'abs_first_i', 'core_length',
                     'core_list'] + addl_annoying_cols
    scored_cols = list(df_apply.columns.values)
    for annoying_col in annoying_cols:
        if annoying_col in scored_cols:
            del df_apply[annoying_col]

        
    return df_apply


def validate_peptides(df_in=None):
    """
    Validate that input peptides contain allowable characters and are long enough to score.
    """

    allowed_aas = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
                   'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V',
                   'W', 'Y']

    assert 'peptide' in list(df_in.columns.values), 'please indicate peptide column in input...'

    # ok basically, for each peptide, count all the characters that aren't in the list
    # of allowable characters.
    df_in['bad_chars'] = \
        df_in['peptide'].apply(lambda x: len([y for y in list(x) if y not in allowed_aas]))

    # discard all of these
    shape_0 = df_in.shape[0]
    df_in = df_in.loc[df_in['bad_chars'] == 0]
    shape_1 = df_in.shape[0]

    n_drops = shape_0 - shape_1
    if n_drops > 0:
        print('\nDROPPING %d PEPTIDES DUE TO NOT ALLOWED CHARACTERS!\n' % n_drops)
    del df_in['bad_chars']
    
    # ok we should apply some kind of length restriction
    df_in['pep_len'] = df_in['peptide'].apply(lambda x: len(x))
    df_in = df_in.loc[df_in['pep_len'] >= 9]
    shape_2 = df_in.shape[0]

    n_drops = shape_1 - shape_2
    if n_drops > 0:
        print('\nDROPPING %d PEPTIDES THAT HAVE 8 OR FEWER AMINO ACIDS!\n' % n_drops)
    del df_in['pep_len']


    return df_in


def norm_inputs_from_disk(test_array=None, base_dir='', allele=''):
    """
    If we had the foresight to save the feature norms and stdds,
    then we don't need to load/featurize the train set to normalize
    our inputs.
    """

    addl_char = ''
    if len(base_dir) > 1:
        if base_dir[-1] != '/':
            base_dir += '/'
            
    feat_params_file = base_dir + addl_char + allele + '_feat_normalization_params.csv'
        
        
    # check if the file actually exists
    success = os.path.isfile(feat_params_file)
    if success:
        df = pd.read_csv(feat_params_file, sep=',')
        for i in range(df.shape[0]):
            the_mean = df.iloc[i]['feat_mean']
            the_stdd = df.iloc[i]['feat_stdd']
            test_array[:, :, i] = (test_array[:, :, i] - the_mean) / (the_stdd + .00001)
    else:
        print('unable to located saved feature statistics...')
        print('we are probably going to calculate them instead...')

    return test_array, success
            

def do_segmentation(val_scores=None, val_labels=None, the_bins=10,
                    dirtag='', filetag='', bin_size=None):
    """
    Perform segmentation with predictions/labels, the output will be
    fed into an isotonic regression to get binding "probabilities"
    """

    if val_scores.shape[-1] == 1:
        val_scores = np.ravel(val_scores)
    if val_labels.shape[-1] == 1:
        val_labels = np.ravel(val_labels)
        
    # option to set number of bins such that they each have a set number
    # of examples in them
    if bin_size is not None:
        n_examples = np.shape(val_scores)[0]
        the_bins = n_examples // bin_size
    
    # ok, we can either pass in the actual bins or pass in the number of bins
    if type(the_bins) == int:
        # assume we received the number of bins
        first_bin = 100.0/the_bins
        last_bin = 100.0
        the_bins = np.linspace(first_bin, last_bin, the_bins)
    
    bin_edges = np.percentile(val_scores, the_bins)
    bin_means = np.zeros(np.shape(bin_edges)[0])
    score_means = np.zeros(np.shape(bin_edges)[0])
    for i in range(bin_edges.shape[0]):
        if i == 0:
            bin_means[i] = val_labels[val_scores <= bin_edges[i]].mean()
            score_means[i] = val_scores[val_scores <= bin_edges[i]].mean()
        else:
            bin_means[i] = val_labels[(val_scores > bin_edges[i-1]) &
                                      (val_scores <= bin_edges[i])].mean()
            score_means[i] = val_scores[(val_scores > bin_edges[i-1]) &
                                        (val_scores <= bin_edges[i])].mean()
            
    return score_means, bin_means
                            

def plot_isotonic_regression(val_scores=None, val_labels=None, filetag='', dirtag='',
                             do_plot=False):
    """
    Given some validation scores and labels, try to fit an isotonic regression
    """
    
    ir = IsotonicRegression()
    
    if val_scores.shape[-1] == 1:
        val_scores = np.ravel(val_scores)
    if val_labels.shape[-1] == 1:
        val_labels = np.ravel(val_labels)
    y_ = ir.fit_transform(val_scores, val_labels)

    return val_scores, y_
                    

if __name__ == "__main__":
    score_wrapper()
